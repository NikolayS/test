                                                                                                  QUERY PLAN
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 Subquery Scan on members  (cost=5181.39..5181.97 rows=33 width=168) (actual time=2.526..2.990 rows=770 loops=1)
   Buffers: shared hit=788
   ->  Unique  (cost=5181.39..5181.64 rows=33 width=172) (actual time=2.524..2.829 rows=770 loops=1)
         Buffers: shared hit=788
         ->  Sort  (cost=5181.39..5181.47 rows=33 width=172) (actual time=2.524..2.611 rows=805 loops=1)
               Sort Key: members_1.user_id, members_1.invite_email, (CASE WHEN ((members_1.type)::text = 'ProjectMember'::text) THEN 1 WHEN ((members_1.type)::text = 'GroupMember'::text) THEN 2 ELSE 3 END)
               Sort Method: quicksort  Memory: 158kB
               Buffers: shared hit=788
               ->  Result  (cost=0.43..5180.56 rows=33 width=172) (actual time=0.037..1.940 rows=805 loops=1)
                     Buffers: shared hit=788
                     ->  Append  (cost=0.43..5180.06 rows=33 width=168) (actual time=0.036..1.664 rows=805 loops=1)
                           Buffers: shared hit=788
                           ->  Append  (cost=0.43..5173.24 rows=30 width=168) (actual time=0.035..1.463 rows=754 loops=1)
                                 Buffers: shared hit=734
                                 ->  Index Scan using index_members_on_source_id_and_source_type on members members_1  (cost=0.43..157.22 rows=28 width=168) (actual time=0.035..1.350 rows=754 loops=1)
                                       Index Cond: ((source_id = 9970) AND ((source_type)::text = 'Namespace'::text))
                                       Filter: ((requested_at IS NULL) AND (invite_token IS NULL) AND ((type)::text = 'GroupMember'::text))
                                       Rows Removed by Filter: 56
                                       Buffers: shared hit=730
                                 ->  Nested Loop  (cost=571.80..5015.71 rows=2 width=168) (actual time=0.019..0.019 rows=0 loops=1)
                                       Buffers: shared hit=4
                                       ->  HashAggregate  (cost=352.81..353.01 rows=21 width=4) (actual time=0.019..0.019 rows=0 loops=1)
                                             Group Key: namespaces.id
                                             Buffers: shared hit=4
                                             ->  CTE Scan on base_and_ancestors namespaces  (cost=352.12..352.54 rows=21 width=4) (actual time=0.017..0.017 rows=0 loops=1)
                                                   Buffers: shared hit=4
                                                   CTE base_and_ancestors
                                                     ->  Recursive Union  (cost=0.43..352.12 rows=21 width=309) (actual time=0.016..0.016 rows=0 loops=1)
                                                           Buffers: shared hit=4
                                                           ->  Index Scan using namespaces_pkey on namespaces namespaces_1  (cost=0.43..3.45 rows=1 width=309) (actual time=0.013..0.013 rows=0 loops=1)
                                                                 Index Cond: (id = 7)
                                                                 Filter: ((type)::text = 'Group'::text)
                                                                 Rows Removed by Filter: 1
                                                                 Buffers: shared hit=4
                                                           ->  Nested Loop  (cost=0.43..34.83 rows=2 width=309) (actual time=0.003..0.003 rows=0 loops=1)
                                                                 ->  WorkTable Scan on base_and_ancestors  (cost=0.00..0.20 rows=10 width=4) (actual time=0.002..0.002 rows=0 loops=1)
                                                                 ->  Index Scan using namespaces_pkey on namespaces namespaces_2  (cost=0.43..3.45 rows=1 width=309) (never executed)
                                                                       Index Cond: (id = base_and_ancestors.parent_id)
                                                                       Filter: ((type)::text = 'Group'::text)
                                       ->  Index Scan using index_members_on_source_id_and_source_type on members members_2  (cost=219.00..222.02 rows=1 width=168) (never executed)
                                             Index Cond: ((source_id = namespaces.id) AND ((source_type)::text = 'Namespace'::text))
                                             Filter: ((requested_at IS NULL) AND (invite_token IS NULL) AND (NOT (hashed SubPlan 1)) AND ((type)::text = 'GroupMember'::text))
                                             SubPlan 1
                                               ->  Nested Loop  (cost=0.86..218.49 rows=29 width=4) (never executed)
                                                     ->  Index Scan using index_members_on_source_id_and_source_type on members members_4  (cost=0.43..157.22 rows=29 width=4) (never executed)
                                                           Index Cond: ((source_id = 9970) AND ((source_type)::text = 'Namespace'::text))
                                                           Filter: ((requested_at IS NULL) AND ((type)::text = 'GroupMember'::text))
                                                     ->  Index Only Scan using users_pkey on users  (cost=0.43..2.10 rows=1 width=4) (never executed)
                                                           Index Cond: (id = members_4.user_id)
                                                           Heap Fetches: 0
                           ->  Index Scan using index_members_on_source_id_and_source_type on members members_3  (cost=0.43..6.50 rows=3 width=168) (actual time=0.014..0.108 rows=51 loops=1)
                                 Index Cond: ((source_id = 13083) AND ((source_type)::text = 'Project'::text))
                                 Filter: ((requested_at IS NULL) AND ((type)::text = 'ProjectMember'::text))
                                 Buffers: shared hit=54
 Planning time: 3.455 ms
 Execution time: 3.384 ms