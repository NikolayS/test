SELECT "members".*
FROM (
  SELECT DISTINCT ON (user_id, invite_email) member_union.*
  FROM (
    SELECT "members".*
    FROM (
      SELECT "members".*
      FROM  "members"
      WHERE
        "members"."type" IN ('GroupMember')
        AND "members"."source_id" = 9970
        AND "members"."source_type" = 'Namespace'
        AND "members"."requested_at" IS NULL

      UNION ALL

      SELECT "members".*
      FROM "members"
      WHERE
        "members"."type" IN ('GroupMember')
        AND "members"."source_type" = 'Namespace'
        AND "members"."requested_at" IS NULL
        AND "members"."source_id" IN (
          WITH RECURSIVE "base_and_ancestors" AS (
            SELECT "namespaces".*
            FROM "namespaces"
            WHERE
              "namespaces"."type" IN ('Group')
              AND "namespaces"."id" = 7
            
            UNION
            
            SELECT "namespaces".*
            FROM "namespaces", "base_and_ancestors"
            WHERE
              "namespaces"."type" IN ('Group')
              AND "namespaces"."id" = "base_and_ancestors"."parent_id"
          )
          SELECT "id"
          FROM "base_and_ancestors" AS "namespaces"
        ) AND (
          "members"."user_id" NOT IN (
            SELECT "users"."id"
            FROM "users"
            INNER JOIN "members" ON "users"."id" = "members"."user_id"
            WHERE
              "members"."type" IN ('GroupMember')
              AND "members"."source_type" = 'Namespace'
              AND "members"."source_id" = 9970
              AND "members"."source_type" = 'Namespace'
              AND "members"."requested_at" IS NULL
          )
        )
      ) members
      WHERE
        "members"."type" IN ('GroupMember')
        AND "members"."source_type" = 'Namespace'
        AND "members"."invite_token" IS NULL

      UNION ALL

      SELECT "members".*
      FROM "members"
      WHERE
        "members"."type" IN ('ProjectMember')
        AND "members"."source_id" = 13083
        AND "members"."source_type" = 'Project'
        AND "members"."requested_at" IS NULL
    ) AS member_union
  ORDER BY
    user_id,
    invite_email,
    CASE WHEN TYPE = 'ProjectMember' THEN
        1
    WHEN TYPE = 'GroupMember' THEN
        2
    ELSE
        3
    END
) AS members;