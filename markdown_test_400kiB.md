# PostgreSQL Checkup. Project: 'test_2'. Database: 'test_locks'
## Epoch number: '8'
NOTICE: while most reports describe the “current database”, some of them may contain cluster-wide information describing all databases in the cluster.

Last modified at:  2019-05-09 23:21:40 +0300


<a name="postgres-checkup_top"></a>
### Table of contents ###

[A002 Version Information](#postgres-checkup_A002)  
[A003 Postgres Settings](#postgres-checkup_A003)  
[A004 Cluster Information](#postgres-checkup_A004)  
[A005 Extensions](#postgres-checkup_A005)  
[A006 Postgres Setting Deviations](#postgres-checkup_A006)  
[A007 Altered Settings](#postgres-checkup_A007)  
[D004 pg_stat_statements and pg_stat_kcache Settings](#postgres-checkup_D004)  
[F001 Autovacuum: Current Settings](#postgres-checkup_F001)  
[F002 Autovacuum: Transaction Wraparound Check](#postgres-checkup_F002)  
[F003 Autovacuum: Dead Tuples](#postgres-checkup_F003)  
[F004 Autovacuum: Heap Bloat (Estimated)](#postgres-checkup_F004)  
[F005 Autovacuum: Index Bloat (Estimated)](#postgres-checkup_F005)  
[F008 Autovacuum: Resource Usage](#postgres-checkup_F008)  
[G001 Memory-related Settings](#postgres-checkup_G001)  
[G002 Connections and Current Activity](#postgres-checkup_G002)  
[G003 Timeouts, Locks, Deadlocks](#postgres-checkup_G003)  
[H001 Invalid Indexes](#postgres-checkup_H001)  
[H002 Unused and Redundant Indexes](#postgres-checkup_H002)  
[H003 Non-indexed Foreign Keys](#postgres-checkup_H003)  
[K001 Globally Aggregated Query Metrics](#postgres-checkup_K001)  
[K002 Workload Type ("First Word" Analysis)](#postgres-checkup_K002)  
[K003 Top-50 Queries by total_time](#postgres-checkup_K003)  
[L001 Table Sizes](#postgres-checkup_L001)  
[L003 Integer (int2, int4) Out-of-range Risks in PKs](#postgres-checkup_L003)  

---
<a name="postgres-checkup_A002"></a>
[Table of contents](#postgres-checkup_top)
# A002 Version Information #

## Observations ##
Data collected: 2019-05-09 23:21:02 +0300 MSK  



### Master (`127.0.0.1`) ###

```
PostgreSQL 10.6 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-28), 64-bit
```






## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A003"></a>
[Table of contents](#postgres-checkup_top)
# A003 Postgres Settings #

## Observations ##
Data collected: 2019-05-09 23:21:02 +0300 MSK  



### Master (`127.0.0.1`) ###  
&#9660;&nbsp;Category | Setting | Value | Unit | Pretty value
---------|---------|-------|------|--------------
Autovacuum|[autovacuum](https://postgresqlco.nf/en/doc/param/autovacuum) | on |  | 
Autovacuum|[autovacuum_analyze_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_scale_factor) | 0.1 |  | 
Autovacuum|[autovacuum_analyze_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_threshold) | 50 |  | 
Autovacuum|[autovacuum_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_freeze_max_age) | 1200000000 |  | 
Autovacuum|[autovacuum_max_workers](https://postgresqlco.nf/en/doc/param/autovacuum_max_workers) | 6 |  | 
Autovacuum|[autovacuum_multixact_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_multixact_freeze_max_age) | 400000000 |  | 
Autovacuum|[autovacuum_naptime](https://postgresqlco.nf/en/doc/param/autovacuum_naptime) | 60 | s  | 
Autovacuum|[autovacuum_vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_delay) | 10 | ms  | 
Autovacuum|[autovacuum_vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_limit) | 5000 |  | 
Autovacuum|[autovacuum_vacuum_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_scale_factor) | 0.2 |  | 
Autovacuum|[autovacuum_vacuum_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_threshold) | 50 |  | 
Client Connection Defaults / Locale and Formatting|[client_encoding](https://postgresqlco.nf/en/doc/param/client_encoding) | UTF8 |  | 
Client Connection Defaults / Locale and Formatting|[DateStyle](https://postgresqlco.nf/en/doc/param/DateStyle) | ISO,  MDY |  | 
Client Connection Defaults / Locale and Formatting|[default_text_search_config](https://postgresqlco.nf/en/doc/param/default_text_search_config) | pg_catalog.english |  | 
Client Connection Defaults / Locale and Formatting|[extra_float_digits](https://postgresqlco.nf/en/doc/param/extra_float_digits) | 0 |  | 
Client Connection Defaults / Locale and Formatting|[IntervalStyle](https://postgresqlco.nf/en/doc/param/IntervalStyle) | postgres |  | 
Client Connection Defaults / Locale and Formatting|[lc_collate](https://postgresqlco.nf/en/doc/param/lc_collate) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_ctype](https://postgresqlco.nf/en/doc/param/lc_ctype) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_messages](https://postgresqlco.nf/en/doc/param/lc_messages) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_monetary](https://postgresqlco.nf/en/doc/param/lc_monetary) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_numeric](https://postgresqlco.nf/en/doc/param/lc_numeric) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_time](https://postgresqlco.nf/en/doc/param/lc_time) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[server_encoding](https://postgresqlco.nf/en/doc/param/server_encoding) | UTF8 |  | 
Client Connection Defaults / Locale and Formatting|[TimeZone](https://postgresqlco.nf/en/doc/param/TimeZone) | W-SU |  | 
Client Connection Defaults / Locale and Formatting|[timezone_abbreviations](https://postgresqlco.nf/en/doc/param/timezone_abbreviations) | Default |  | 
Client Connection Defaults / Other Defaults|[dynamic_library_path](https://postgresqlco.nf/en/doc/param/dynamic_library_path) | $libdir |  | 
Client Connection Defaults / Other Defaults|[gin_fuzzy_search_limit](https://postgresqlco.nf/en/doc/param/gin_fuzzy_search_limit) | 0 |  | 
Client Connection Defaults / Other Defaults|[tcp_keepalives_count](https://postgresqlco.nf/en/doc/param/tcp_keepalives_count) | 0 |  | 
Client Connection Defaults / Other Defaults|[tcp_keepalives_idle](https://postgresqlco.nf/en/doc/param/tcp_keepalives_idle) | 0 | s  | 
Client Connection Defaults / Other Defaults|[tcp_keepalives_interval](https://postgresqlco.nf/en/doc/param/tcp_keepalives_interval) | 0 | s  | 
Client Connection Defaults / Shared Library Preloading|[local_preload_libraries](https://postgresqlco.nf/en/doc/param/local_preload_libraries) |  |  | 
Client Connection Defaults / Shared Library Preloading|[session_preload_libraries](https://postgresqlco.nf/en/doc/param/session_preload_libraries) |  |  | 
Client Connection Defaults / Shared Library Preloading|[shared_preload_libraries](https://postgresqlco.nf/en/doc/param/shared_preload_libraries) | pg_stat_statements |  | 
Client Connection Defaults / Statement Behavior|[bytea_output](https://postgresqlco.nf/en/doc/param/bytea_output) | hex |  | 
Client Connection Defaults / Statement Behavior|[check_function_bodies](https://postgresqlco.nf/en/doc/param/check_function_bodies) | on |  | 
Client Connection Defaults / Statement Behavior|[default_tablespace](https://postgresqlco.nf/en/doc/param/default_tablespace) |  |  | 
Client Connection Defaults / Statement Behavior|[default_transaction_deferrable](https://postgresqlco.nf/en/doc/param/default_transaction_deferrable) | off |  | 
Client Connection Defaults / Statement Behavior|[default_transaction_isolation](https://postgresqlco.nf/en/doc/param/default_transaction_isolation) | read committed |  | 
Client Connection Defaults / Statement Behavior|[default_transaction_read_only](https://postgresqlco.nf/en/doc/param/default_transaction_read_only) | off |  | 
Client Connection Defaults / Statement Behavior|[gin_pending_list_limit](https://postgresqlco.nf/en/doc/param/gin_pending_list_limit) | 4096 | kB  | 4.00 MiB
Client Connection Defaults / Statement Behavior|[idle_in_transaction_session_timeout](https://postgresqlco.nf/en/doc/param/idle_in_transaction_session_timeout) | 0 | ms  | 
Client Connection Defaults / Statement Behavior|[lock_timeout](https://postgresqlco.nf/en/doc/param/lock_timeout) | 0 | ms  | 
Client Connection Defaults / Statement Behavior|[search_path](https://postgresqlco.nf/en/doc/param/search_path) | "$user",  public |  | 
Client Connection Defaults / Statement Behavior|[session_replication_role](https://postgresqlco.nf/en/doc/param/session_replication_role) | origin |  | 
Client Connection Defaults / Statement Behavior|[statement_timeout](https://postgresqlco.nf/en/doc/param/statement_timeout) | 30000 | ms  | 
Client Connection Defaults / Statement Behavior|[temp_tablespaces](https://postgresqlco.nf/en/doc/param/temp_tablespaces) |  |  | 
Client Connection Defaults / Statement Behavior|[transaction_deferrable](https://postgresqlco.nf/en/doc/param/transaction_deferrable) | off |  | 
Client Connection Defaults / Statement Behavior|[transaction_isolation](https://postgresqlco.nf/en/doc/param/transaction_isolation) | read committed |  | 
Client Connection Defaults / Statement Behavior|[transaction_read_only](https://postgresqlco.nf/en/doc/param/transaction_read_only) | off |  | 
Client Connection Defaults / Statement Behavior|[vacuum_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_min_age) | 50000000 |  | 
Client Connection Defaults / Statement Behavior|[vacuum_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_table_age) | 150000000 |  | 
Client Connection Defaults / Statement Behavior|[vacuum_multixact_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_min_age) | 5000000 |  | 
Client Connection Defaults / Statement Behavior|[vacuum_multixact_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_table_age) | 150000000 |  | 
Client Connection Defaults / Statement Behavior|[xmlbinary](https://postgresqlco.nf/en/doc/param/xmlbinary) | base64 |  | 
Client Connection Defaults / Statement Behavior|[xmloption](https://postgresqlco.nf/en/doc/param/xmloption) | content |  | 
Connections and Authentication / Connection Settings|[bonjour](https://postgresqlco.nf/en/doc/param/bonjour) | off |  | 
Connections and Authentication / Connection Settings|[bonjour_name](https://postgresqlco.nf/en/doc/param/bonjour_name) |  |  | 
Connections and Authentication / Connection Settings|[listen_addresses](https://postgresqlco.nf/en/doc/param/listen_addresses) | localhost |  | 
Connections and Authentication / Connection Settings|[max_connections](https://postgresqlco.nf/en/doc/param/max_connections) | 100 |  | 
Connections and Authentication / Connection Settings|[port](https://postgresqlco.nf/en/doc/param/port) | 5432 |  | 
Connections and Authentication / Connection Settings|[superuser_reserved_connections](https://postgresqlco.nf/en/doc/param/superuser_reserved_connections) | 3 |  | 
Connections and Authentication / Connection Settings|[unix_socket_directories](https://postgresqlco.nf/en/doc/param/unix_socket_directories) | /var/run/postgresql,  /tmp |  | 
Connections and Authentication / Connection Settings|[unix_socket_group](https://postgresqlco.nf/en/doc/param/unix_socket_group) |  |  | 
Connections and Authentication / Connection Settings|[unix_socket_permissions](https://postgresqlco.nf/en/doc/param/unix_socket_permissions) | 0777 |  | 
Connections and Authentication / Security and Authentication|[authentication_timeout](https://postgresqlco.nf/en/doc/param/authentication_timeout) | 60 | s  | 
Connections and Authentication / Security and Authentication|[db_user_namespace](https://postgresqlco.nf/en/doc/param/db_user_namespace) | off |  | 
Connections and Authentication / Security and Authentication|[krb_caseins_users](https://postgresqlco.nf/en/doc/param/krb_caseins_users) | off |  | 
Connections and Authentication / Security and Authentication|[krb_server_keyfile](https://postgresqlco.nf/en/doc/param/krb_server_keyfile) | FILE:/etc/sysconfig/pgsql/krb5.keytab |  | 
Connections and Authentication / Security and Authentication|[password_encryption](https://postgresqlco.nf/en/doc/param/password_encryption) | md5 |  | 
Connections and Authentication / Security and Authentication|[row_security](https://postgresqlco.nf/en/doc/param/row_security) | on |  | 
Connections and Authentication / Security and Authentication|[ssl](https://postgresqlco.nf/en/doc/param/ssl) | off |  | 
Connections and Authentication / Security and Authentication|[ssl_ca_file](https://postgresqlco.nf/en/doc/param/ssl_ca_file) |  |  | 
Connections and Authentication / Security and Authentication|[ssl_cert_file](https://postgresqlco.nf/en/doc/param/ssl_cert_file) | server.crt |  | 
Connections and Authentication / Security and Authentication|[ssl_ciphers](https://postgresqlco.nf/en/doc/param/ssl_ciphers) | HIGH:MEDIUM:+3DES:!aNULL |  | 
Connections and Authentication / Security and Authentication|[ssl_crl_file](https://postgresqlco.nf/en/doc/param/ssl_crl_file) |  |  | 
Connections and Authentication / Security and Authentication|[ssl_dh_params_file](https://postgresqlco.nf/en/doc/param/ssl_dh_params_file) |  |  | 
Connections and Authentication / Security and Authentication|[ssl_ecdh_curve](https://postgresqlco.nf/en/doc/param/ssl_ecdh_curve) | prime256v1 |  | 
Connections and Authentication / Security and Authentication|[ssl_key_file](https://postgresqlco.nf/en/doc/param/ssl_key_file) | server.key |  | 
Connections and Authentication / Security and Authentication|[ssl_prefer_server_ciphers](https://postgresqlco.nf/en/doc/param/ssl_prefer_server_ciphers) | on |  | 
Customized Options|[pg_stat_statements.max](https://postgresqlco.nf/en/doc/param/pg_stat_statements.max) | 5000 |  | 
Customized Options|[pg_stat_statements.save](https://postgresqlco.nf/en/doc/param/pg_stat_statements.save) | on |  | 
Customized Options|[pg_stat_statements.track](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track) | top |  | 
Customized Options|[pg_stat_statements.track_utility](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track_utility) | on |  | 
Developer Options|[allow_system_table_mods](https://postgresqlco.nf/en/doc/param/allow_system_table_mods) | off |  | 
Developer Options|[ignore_checksum_failure](https://postgresqlco.nf/en/doc/param/ignore_checksum_failure) | off |  | 
Developer Options|[ignore_system_indexes](https://postgresqlco.nf/en/doc/param/ignore_system_indexes) | off |  | 
Developer Options|[post_auth_delay](https://postgresqlco.nf/en/doc/param/post_auth_delay) | 0 | s  | 
Developer Options|[pre_auth_delay](https://postgresqlco.nf/en/doc/param/pre_auth_delay) | 0 | s  | 
Developer Options|[trace_notify](https://postgresqlco.nf/en/doc/param/trace_notify) | off |  | 
Developer Options|[trace_recovery_messages](https://postgresqlco.nf/en/doc/param/trace_recovery_messages) | log |  | 
Developer Options|[trace_sort](https://postgresqlco.nf/en/doc/param/trace_sort) | off |  | 
Developer Options|[wal_consistency_checking](https://postgresqlco.nf/en/doc/param/wal_consistency_checking) |  |  | 
Developer Options|[zero_damaged_pages](https://postgresqlco.nf/en/doc/param/zero_damaged_pages) | off |  | 
Error Handling|[exit_on_error](https://postgresqlco.nf/en/doc/param/exit_on_error) | off |  | 
Error Handling|[restart_after_crash](https://postgresqlco.nf/en/doc/param/restart_after_crash) | on |  | 
File Locations|[config_file](https://postgresqlco.nf/en/doc/param/config_file) | /home/db_main_node/postgresql.conf |  | 
File Locations|[data_directory](https://postgresqlco.nf/en/doc/param/data_directory) | /home/db_main_node |  | 
File Locations|[external_pid_file](https://postgresqlco.nf/en/doc/param/external_pid_file) |  |  | 
File Locations|[hba_file](https://postgresqlco.nf/en/doc/param/hba_file) | /home/db_main_node/pg_hba.conf |  | 
File Locations|[ident_file](https://postgresqlco.nf/en/doc/param/ident_file) | /home/db_main_node/pg_ident.conf |  | 
Lock Management|[deadlock_timeout](https://postgresqlco.nf/en/doc/param/deadlock_timeout) | 1000 | ms  | 
Lock Management|[max_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_locks_per_transaction) | 64 |  | 
Lock Management|[max_pred_locks_per_page](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_page) | 2 |  | 
Lock Management|[max_pred_locks_per_relation](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_relation) | -2 |  | 
Lock Management|[max_pred_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_transaction) | 64 |  | 
Preset Options|[block_size](https://postgresqlco.nf/en/doc/param/block_size) | 8192 |  | 
Preset Options|[data_checksums](https://postgresqlco.nf/en/doc/param/data_checksums) | off |  | 
Preset Options|[debug_assertions](https://postgresqlco.nf/en/doc/param/debug_assertions) | off |  | 
Preset Options|[integer_datetimes](https://postgresqlco.nf/en/doc/param/integer_datetimes) | on |  | 
Preset Options|[max_function_args](https://postgresqlco.nf/en/doc/param/max_function_args) | 100 |  | 
Preset Options|[max_identifier_length](https://postgresqlco.nf/en/doc/param/max_identifier_length) | 63 |  | 
Preset Options|[max_index_keys](https://postgresqlco.nf/en/doc/param/max_index_keys) | 32 |  | 
Preset Options|[segment_size](https://postgresqlco.nf/en/doc/param/segment_size) | 131072 | 8kB  | 1.00 GiB
Preset Options|[server_version](https://postgresqlco.nf/en/doc/param/server_version) | 10.6 |  | 
Preset Options|[server_version_num](https://postgresqlco.nf/en/doc/param/server_version_num) | 100006 |  | 
Preset Options|[wal_block_size](https://postgresqlco.nf/en/doc/param/wal_block_size) | 8192 |  | 
Preset Options|[wal_segment_size](https://postgresqlco.nf/en/doc/param/wal_segment_size) | 2048 | 8kB  | 16.00 MiB
Process Title|[cluster_name](https://postgresqlco.nf/en/doc/param/cluster_name) |  |  | 
Process Title|[update_process_title](https://postgresqlco.nf/en/doc/param/update_process_title) | on |  | 
Query Tuning / Genetic Query Optimizer|[geqo](https://postgresqlco.nf/en/doc/param/geqo) | on |  | 
Query Tuning / Genetic Query Optimizer|[geqo_effort](https://postgresqlco.nf/en/doc/param/geqo_effort) | 5 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_generations](https://postgresqlco.nf/en/doc/param/geqo_generations) | 0 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_pool_size](https://postgresqlco.nf/en/doc/param/geqo_pool_size) | 0 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_seed](https://postgresqlco.nf/en/doc/param/geqo_seed) | 0 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_selection_bias](https://postgresqlco.nf/en/doc/param/geqo_selection_bias) | 2 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_threshold](https://postgresqlco.nf/en/doc/param/geqo_threshold) | 12 |  | 
Query Tuning / Other Planner Options|[constraint_exclusion](https://postgresqlco.nf/en/doc/param/constraint_exclusion) | partition |  | 
Query Tuning / Other Planner Options|[cursor_tuple_fraction](https://postgresqlco.nf/en/doc/param/cursor_tuple_fraction) | 0.1 |  | 
Query Tuning / Other Planner Options|[default_statistics_target](https://postgresqlco.nf/en/doc/param/default_statistics_target) | 100 |  | 
Query Tuning / Other Planner Options|[force_parallel_mode](https://postgresqlco.nf/en/doc/param/force_parallel_mode) | off |  | 
Query Tuning / Other Planner Options|[from_collapse_limit](https://postgresqlco.nf/en/doc/param/from_collapse_limit) | 8 |  | 
Query Tuning / Other Planner Options|[join_collapse_limit](https://postgresqlco.nf/en/doc/param/join_collapse_limit) | 8 |  | 
Query Tuning / Planner Cost Constants|[cpu_index_tuple_cost](https://postgresqlco.nf/en/doc/param/cpu_index_tuple_cost) | 0.005 |  | 
Query Tuning / Planner Cost Constants|[cpu_operator_cost](https://postgresqlco.nf/en/doc/param/cpu_operator_cost) | 0.0025 |  | 
Query Tuning / Planner Cost Constants|[cpu_tuple_cost](https://postgresqlco.nf/en/doc/param/cpu_tuple_cost) | 0.01 |  | 
Query Tuning / Planner Cost Constants|[effective_cache_size](https://postgresqlco.nf/en/doc/param/effective_cache_size) | 524288 | 8kB  | 4.00 GiB
Query Tuning / Planner Cost Constants|[min_parallel_index_scan_size](https://postgresqlco.nf/en/doc/param/min_parallel_index_scan_size) | 64 | 8kB  | 512.00 KiB
Query Tuning / Planner Cost Constants|[min_parallel_table_scan_size](https://postgresqlco.nf/en/doc/param/min_parallel_table_scan_size) | 1024 | 8kB  | 8.00 MiB
Query Tuning / Planner Cost Constants|[parallel_setup_cost](https://postgresqlco.nf/en/doc/param/parallel_setup_cost) | 1000 |  | 
Query Tuning / Planner Cost Constants|[parallel_tuple_cost](https://postgresqlco.nf/en/doc/param/parallel_tuple_cost) | 0.1 |  | 
Query Tuning / Planner Cost Constants|[random_page_cost](https://postgresqlco.nf/en/doc/param/random_page_cost) | 2.22 |  | 
Query Tuning / Planner Cost Constants|[seq_page_cost](https://postgresqlco.nf/en/doc/param/seq_page_cost) | 1 |  | 
Query Tuning / Planner Method Configuration|[enable_bitmapscan](https://postgresqlco.nf/en/doc/param/enable_bitmapscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_gathermerge](https://postgresqlco.nf/en/doc/param/enable_gathermerge) | on |  | 
Query Tuning / Planner Method Configuration|[enable_hashagg](https://postgresqlco.nf/en/doc/param/enable_hashagg) | on |  | 
Query Tuning / Planner Method Configuration|[enable_hashjoin](https://postgresqlco.nf/en/doc/param/enable_hashjoin) | on |  | 
Query Tuning / Planner Method Configuration|[enable_indexonlyscan](https://postgresqlco.nf/en/doc/param/enable_indexonlyscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_indexscan](https://postgresqlco.nf/en/doc/param/enable_indexscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_material](https://postgresqlco.nf/en/doc/param/enable_material) | on |  | 
Query Tuning / Planner Method Configuration|[enable_mergejoin](https://postgresqlco.nf/en/doc/param/enable_mergejoin) | on |  | 
Query Tuning / Planner Method Configuration|[enable_nestloop](https://postgresqlco.nf/en/doc/param/enable_nestloop) | on |  | 
Query Tuning / Planner Method Configuration|[enable_seqscan](https://postgresqlco.nf/en/doc/param/enable_seqscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_sort](https://postgresqlco.nf/en/doc/param/enable_sort) | on |  | 
Query Tuning / Planner Method Configuration|[enable_tidscan](https://postgresqlco.nf/en/doc/param/enable_tidscan) | on |  | 
Replication|[track_commit_timestamp](https://postgresqlco.nf/en/doc/param/track_commit_timestamp) | off |  | 
Replication / Master Server|[synchronous_standby_names](https://postgresqlco.nf/en/doc/param/synchronous_standby_names) |  |  | 
Replication / Master Server|[vacuum_defer_cleanup_age](https://postgresqlco.nf/en/doc/param/vacuum_defer_cleanup_age) | 0 |  | 
Replication / Sending Servers|[max_replication_slots](https://postgresqlco.nf/en/doc/param/max_replication_slots) | 10 |  | 
Replication / Sending Servers|[max_wal_senders](https://postgresqlco.nf/en/doc/param/max_wal_senders) | 0 |  | 
Replication / Sending Servers|[wal_keep_segments](https://postgresqlco.nf/en/doc/param/wal_keep_segments) | 0 |  | 
Replication / Sending Servers|[wal_sender_timeout](https://postgresqlco.nf/en/doc/param/wal_sender_timeout) | 60000 | ms  | 
Replication / Standby Servers|[hot_standby](https://postgresqlco.nf/en/doc/param/hot_standby) | on |  | 
Replication / Standby Servers|[hot_standby_feedback](https://postgresqlco.nf/en/doc/param/hot_standby_feedback) | off |  | 
Replication / Standby Servers|[max_standby_archive_delay](https://postgresqlco.nf/en/doc/param/max_standby_archive_delay) | 30000 | ms  | 
Replication / Standby Servers|[max_standby_streaming_delay](https://postgresqlco.nf/en/doc/param/max_standby_streaming_delay) | 30000 | ms  | 
Replication / Standby Servers|[wal_receiver_status_interval](https://postgresqlco.nf/en/doc/param/wal_receiver_status_interval) | 10 | s  | 
Replication / Standby Servers|[wal_receiver_timeout](https://postgresqlco.nf/en/doc/param/wal_receiver_timeout) | 60000 | ms  | 
Replication / Standby Servers|[wal_retrieve_retry_interval](https://postgresqlco.nf/en/doc/param/wal_retrieve_retry_interval) | 5000 | ms  | 
Replication / Subscribers|[max_logical_replication_workers](https://postgresqlco.nf/en/doc/param/max_logical_replication_workers) | 4 |  | 
Replication / Subscribers|[max_sync_workers_per_subscription](https://postgresqlco.nf/en/doc/param/max_sync_workers_per_subscription) | 2 |  | 
Reporting and Logging / What to Log|[application_name](https://postgresqlco.nf/en/doc/param/application_name) | psql |  | 
Reporting and Logging / What to Log|[debug_pretty_print](https://postgresqlco.nf/en/doc/param/debug_pretty_print) | on |  | 
Reporting and Logging / What to Log|[debug_print_parse](https://postgresqlco.nf/en/doc/param/debug_print_parse) | off |  | 
Reporting and Logging / What to Log|[debug_print_plan](https://postgresqlco.nf/en/doc/param/debug_print_plan) | off |  | 
Reporting and Logging / What to Log|[debug_print_rewritten](https://postgresqlco.nf/en/doc/param/debug_print_rewritten) | off |  | 
Reporting and Logging / What to Log|[log_autovacuum_min_duration](https://postgresqlco.nf/en/doc/param/log_autovacuum_min_duration) | 1000 | ms  | 
Reporting and Logging / What to Log|[log_checkpoints](https://postgresqlco.nf/en/doc/param/log_checkpoints) | off |  | 
Reporting and Logging / What to Log|[log_connections](https://postgresqlco.nf/en/doc/param/log_connections) | on |  | 
Reporting and Logging / What to Log|[log_disconnections](https://postgresqlco.nf/en/doc/param/log_disconnections) | on |  | 
Reporting and Logging / What to Log|[log_duration](https://postgresqlco.nf/en/doc/param/log_duration) | off |  | 
Reporting and Logging / What to Log|[log_error_verbosity](https://postgresqlco.nf/en/doc/param/log_error_verbosity) | default |  | 
Reporting and Logging / What to Log|[log_hostname](https://postgresqlco.nf/en/doc/param/log_hostname) | off |  | 
Reporting and Logging / What to Log|[log_line_prefix](https://postgresqlco.nf/en/doc/param/log_line_prefix) | %m|%u|%d|%c| |  | 
Reporting and Logging / What to Log|[log_lock_waits](https://postgresqlco.nf/en/doc/param/log_lock_waits) | off |  | 
Reporting and Logging / What to Log|[log_replication_commands](https://postgresqlco.nf/en/doc/param/log_replication_commands) | off |  | 
Reporting and Logging / What to Log|[log_statement](https://postgresqlco.nf/en/doc/param/log_statement) | all |  | 
Reporting and Logging / What to Log|[log_temp_files](https://postgresqlco.nf/en/doc/param/log_temp_files) | -1 | kB  | 
Reporting and Logging / What to Log|[log_timezone](https://postgresqlco.nf/en/doc/param/log_timezone) | W-SU |  | 
Reporting and Logging / When to Log|[client_min_messages](https://postgresqlco.nf/en/doc/param/client_min_messages) | notice |  | 
Reporting and Logging / When to Log|[log_min_duration_statement](https://postgresqlco.nf/en/doc/param/log_min_duration_statement) | 0 | ms  | 
Reporting and Logging / When to Log|[log_min_error_statement](https://postgresqlco.nf/en/doc/param/log_min_error_statement) | log |  | 
Reporting and Logging / When to Log|[log_min_messages](https://postgresqlco.nf/en/doc/param/log_min_messages) | error |  | 
Reporting and Logging / Where to Log|[event_source](https://postgresqlco.nf/en/doc/param/event_source) | PostgreSQL |  | 
Reporting and Logging / Where to Log|[log_destination](https://postgresqlco.nf/en/doc/param/log_destination) | stderr |  | 
Reporting and Logging / Where to Log|[log_directory](https://postgresqlco.nf/en/doc/param/log_directory) | log |  | 
Reporting and Logging / Where to Log|[log_file_mode](https://postgresqlco.nf/en/doc/param/log_file_mode) | 0600 |  | 
Reporting and Logging / Where to Log|[log_filename](https://postgresqlco.nf/en/doc/param/log_filename) | postgresql-%a.log |  | 
Reporting and Logging / Where to Log|[logging_collector](https://postgresqlco.nf/en/doc/param/logging_collector) | on |  | 
Reporting and Logging / Where to Log|[log_rotation_age](https://postgresqlco.nf/en/doc/param/log_rotation_age) | 1440 | min  | 
Reporting and Logging / Where to Log|[log_rotation_size](https://postgresqlco.nf/en/doc/param/log_rotation_size) | 0 | kB  | 0.00 bytes
Reporting and Logging / Where to Log|[log_truncate_on_rotation](https://postgresqlco.nf/en/doc/param/log_truncate_on_rotation) | on |  | 
Reporting and Logging / Where to Log|[syslog_facility](https://postgresqlco.nf/en/doc/param/syslog_facility) | local0 |  | 
Reporting and Logging / Where to Log|[syslog_ident](https://postgresqlco.nf/en/doc/param/syslog_ident) | postgres |  | 
Reporting and Logging / Where to Log|[syslog_sequence_numbers](https://postgresqlco.nf/en/doc/param/syslog_sequence_numbers) | on |  | 
Reporting and Logging / Where to Log|[syslog_split_messages](https://postgresqlco.nf/en/doc/param/syslog_split_messages) | on |  | 
Resource Usage / Asynchronous Behavior|[backend_flush_after](https://postgresqlco.nf/en/doc/param/backend_flush_after) | 0 | 8kB  | 0.00 bytes
Resource Usage / Asynchronous Behavior|[effective_io_concurrency](https://postgresqlco.nf/en/doc/param/effective_io_concurrency) | 1 |  | 
Resource Usage / Asynchronous Behavior|[max_parallel_workers](https://postgresqlco.nf/en/doc/param/max_parallel_workers) | 8 |  | 
Resource Usage / Asynchronous Behavior|[max_parallel_workers_per_gather](https://postgresqlco.nf/en/doc/param/max_parallel_workers_per_gather) | 2 |  | 
Resource Usage / Asynchronous Behavior|[max_worker_processes](https://postgresqlco.nf/en/doc/param/max_worker_processes) | 8 |  | 
Resource Usage / Asynchronous Behavior|[old_snapshot_threshold](https://postgresqlco.nf/en/doc/param/old_snapshot_threshold) | -1 | min  | 
Resource Usage / Background Writer|[bgwriter_delay](https://postgresqlco.nf/en/doc/param/bgwriter_delay) | 100 | ms  | 
Resource Usage / Background Writer|[bgwriter_flush_after](https://postgresqlco.nf/en/doc/param/bgwriter_flush_after) | 189 | 8kB  | 1.48 MiB
Resource Usage / Background Writer|[bgwriter_lru_maxpages](https://postgresqlco.nf/en/doc/param/bgwriter_lru_maxpages) | 1000 |  | 
Resource Usage / Background Writer|[bgwriter_lru_multiplier](https://postgresqlco.nf/en/doc/param/bgwriter_lru_multiplier) | 7 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/vacuum_cost_delay) | 0 | ms  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/vacuum_cost_limit) | 4000 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_page_dirty](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_dirty) | 20 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_page_hit](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_hit) | 1 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_page_miss](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_miss) | 10 |  | 
Resource Usage / Disk|[temp_file_limit](https://postgresqlco.nf/en/doc/param/temp_file_limit) | -1 | kB  | 
Resource Usage / Kernel Resources|[max_files_per_process](https://postgresqlco.nf/en/doc/param/max_files_per_process) | 1000 |  | 
Resource Usage / Memory|[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem) | -1 | kB  | 
Resource Usage / Memory|[dynamic_shared_memory_type](https://postgresqlco.nf/en/doc/param/dynamic_shared_memory_type) | posix |  | 
Resource Usage / Memory|[huge_pages](https://postgresqlco.nf/en/doc/param/huge_pages) | try |  | 
Resource Usage / Memory|[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem) | 270336 | kB  | 264.00 MiB
Resource Usage / Memory|[max_prepared_transactions](https://postgresqlco.nf/en/doc/param/max_prepared_transactions) | 0 |  | 
Resource Usage / Memory|[max_stack_depth](https://postgresqlco.nf/en/doc/param/max_stack_depth) | 2048 | kB  | 2.00 MiB
Resource Usage / Memory|[replacement_sort_tuples](https://postgresqlco.nf/en/doc/param/replacement_sort_tuples) | 150000 |  | 
Resource Usage / Memory|[shared_buffers](https://postgresqlco.nf/en/doc/param/shared_buffers) | 102400 | 8kB  | 800.00 MiB
Resource Usage / Memory|[temp_buffers](https://postgresqlco.nf/en/doc/param/temp_buffers) | 1024 | 8kB  | 8.00 MiB
Resource Usage / Memory|[track_activity_query_size](https://postgresqlco.nf/en/doc/param/track_activity_query_size) | 1024 |  | 
Resource Usage / Memory|[work_mem](https://postgresqlco.nf/en/doc/param/work_mem) | 153600 | kB  | 150.00 MiB
Statistics / Monitoring|[log_executor_stats](https://postgresqlco.nf/en/doc/param/log_executor_stats) | off |  | 
Statistics / Monitoring|[log_parser_stats](https://postgresqlco.nf/en/doc/param/log_parser_stats) | off |  | 
Statistics / Monitoring|[log_planner_stats](https://postgresqlco.nf/en/doc/param/log_planner_stats) | off |  | 
Statistics / Monitoring|[log_statement_stats](https://postgresqlco.nf/en/doc/param/log_statement_stats) | off |  | 
Statistics / Query and Index Statistics Collector|[stats_temp_directory](https://postgresqlco.nf/en/doc/param/stats_temp_directory) | pg_stat_tmp |  | 
Statistics / Query and Index Statistics Collector|[track_activities](https://postgresqlco.nf/en/doc/param/track_activities) | on |  | 
Statistics / Query and Index Statistics Collector|[track_counts](https://postgresqlco.nf/en/doc/param/track_counts) | on |  | 
Statistics / Query and Index Statistics Collector|[track_functions](https://postgresqlco.nf/en/doc/param/track_functions) | none |  | 
Statistics / Query and Index Statistics Collector|[track_io_timing](https://postgresqlco.nf/en/doc/param/track_io_timing) | off |  | 
Version and Platform Compatibility / Other Platforms and Clients|[transform_null_equals](https://postgresqlco.nf/en/doc/param/transform_null_equals) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[array_nulls](https://postgresqlco.nf/en/doc/param/array_nulls) | on |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[backslash_quote](https://postgresqlco.nf/en/doc/param/backslash_quote) | safe_encoding |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[default_with_oids](https://postgresqlco.nf/en/doc/param/default_with_oids) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[escape_string_warning](https://postgresqlco.nf/en/doc/param/escape_string_warning) | on |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[lo_compat_privileges](https://postgresqlco.nf/en/doc/param/lo_compat_privileges) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[operator_precedence_warning](https://postgresqlco.nf/en/doc/param/operator_precedence_warning) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[quote_all_identifiers](https://postgresqlco.nf/en/doc/param/quote_all_identifiers) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[standard_conforming_strings](https://postgresqlco.nf/en/doc/param/standard_conforming_strings) | on |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[synchronize_seqscans](https://postgresqlco.nf/en/doc/param/synchronize_seqscans) | on |  | 
Write-Ahead Log / Archiving|[archive_command](https://postgresqlco.nf/en/doc/param/archive_command) | (disabled) |  | 
Write-Ahead Log / Archiving|[archive_mode](https://postgresqlco.nf/en/doc/param/archive_mode) | off |  | 
Write-Ahead Log / Archiving|[archive_timeout](https://postgresqlco.nf/en/doc/param/archive_timeout) | 0 | s  | 
Write-Ahead Log / Checkpoints|[checkpoint_completion_target](https://postgresqlco.nf/en/doc/param/checkpoint_completion_target) | 0.7 |  | 
Write-Ahead Log / Checkpoints|[checkpoint_flush_after](https://postgresqlco.nf/en/doc/param/checkpoint_flush_after) | 32 | 8kB  | 256.00 KiB
Write-Ahead Log / Checkpoints|[checkpoint_timeout](https://postgresqlco.nf/en/doc/param/checkpoint_timeout) | 600 | s  | 
Write-Ahead Log / Checkpoints|[checkpoint_warning](https://postgresqlco.nf/en/doc/param/checkpoint_warning) | 30 | s  | 
Write-Ahead Log / Checkpoints|[max_wal_size](https://postgresqlco.nf/en/doc/param/max_wal_size) | 1024 | MB  | 1.00 GiB
Write-Ahead Log / Checkpoints|[min_wal_size](https://postgresqlco.nf/en/doc/param/min_wal_size) | 280 | MB  | 280.00 MiB
Write-Ahead Log / Settings|[commit_delay](https://postgresqlco.nf/en/doc/param/commit_delay) | 0 |  | 
Write-Ahead Log / Settings|[commit_siblings](https://postgresqlco.nf/en/doc/param/commit_siblings) | 5 |  | 
Write-Ahead Log / Settings|[fsync](https://postgresqlco.nf/en/doc/param/fsync) | on |  | 
Write-Ahead Log / Settings|[full_page_writes](https://postgresqlco.nf/en/doc/param/full_page_writes) | on |  | 
Write-Ahead Log / Settings|[synchronous_commit](https://postgresqlco.nf/en/doc/param/synchronous_commit) | off |  | 
Write-Ahead Log / Settings|[wal_buffers](https://postgresqlco.nf/en/doc/param/wal_buffers) | 2048 | 8kB  | 16.00 MiB
Write-Ahead Log / Settings|[wal_compression](https://postgresqlco.nf/en/doc/param/wal_compression) | off |  | 
Write-Ahead Log / Settings|[wal_level](https://postgresqlco.nf/en/doc/param/wal_level) | minimal |  | 
Write-Ahead Log / Settings|[wal_log_hints](https://postgresqlco.nf/en/doc/param/wal_log_hints) | off |  | 
Write-Ahead Log / Settings|[wal_sync_method](https://postgresqlco.nf/en/doc/param/wal_sync_method) | fdatasync |  | 
Write-Ahead Log / Settings|[wal_writer_delay](https://postgresqlco.nf/en/doc/param/wal_writer_delay) | 200 | ms  | 
Write-Ahead Log / Settings|[wal_writer_flush_after](https://postgresqlco.nf/en/doc/param/wal_writer_flush_after) | 128 | 8kB  | 1.00 MiB

  
---
<a name="postgres-checkup_A004"></a>
[Table of contents](#postgres-checkup_top)
# A004 Cluster Information #

## Observations ##
Data collected: 2019-05-09 23:21:06 +0300 MSK  

&#9660;&nbsp;Indicator | 127.0.0.1 
--------|-------
Config file |/home/db_main_node/postgresql.conf
Role |Master
Replicas |
Started At |2019-05-06&nbsp;23:41:22+03
Uptime |2&nbsp;days&nbsp;23:39:45
Checkpoints |8040
Forced Checkpoints |10.3%
Checkpoint MB/sec |0.007971
Database Name |test_locks
Database Size |7639&nbsp;MB
Stats Since |2019-05-06&nbsp;23:22:05+03
Stats Age |2&nbsp;days&nbsp;23:59:01
Cache Effectiveness |77.16%
Successful Commits |100.00%
Conflicts |0
Temp Files: total size |956&nbsp;MB
Temp Files: total number of files |1
Temp Files: total number of files per day |0
Temp Files: avg file size |956&nbsp;MB
Deadlocks |0
Deadlocks per day |0


### Databases sizes ###
Database | &#9660;&nbsp;Size
---------|------
test_locks | 7.46&nbsp;GiB
postgres | 7.71&nbsp;MiB
template1 | 7.68&nbsp;MiB
template0 | 7.68&nbsp;MiB


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A005"></a>
[Table of contents](#postgres-checkup_top)
# A005 Extensions #

## Observations ##
Data collected: 2019-05-09 23:21:06 +0300 MSK  



### Master (`127.0.0.1`) ###
&#9660;&nbsp;Database | Extension name | Installed version | Default version | Is old
---------|----------------|-------------------|-----------------|--------
test_locks | pg_stat_statements | 1.6 | 1.6 | <no value>
test_locks | plpgsql | 1.0 | 1.0 | <no value>




## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A006"></a>
[Table of contents](#postgres-checkup_top)
# A006 Postgres Setting Deviations #

## Observations ##
Data collected: 2019-05-09 23:21:06 +0300 MSK  

### Settings (pg_settings) that Differ ###

No differences in `pg_settings` are found.

### Configs(pg_config) that differ ###

No differences in `pg_config` are found.



## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A007"></a>
[Table of contents](#postgres-checkup_top)
# A007 Altered Settings #

## Observations ##
Data collected: 2019-05-09 23:21:10 +0300 MSK  



### Master (`127.0.0.1`) ###
Source | Settings specified | List of settings
-------|--------------------|-----------------
/home/db_main_node/postgresql.auto.conf | 12 |  autovacuum autovacuum_freeze_max_age autovacuum_vacuum_cost_limit log_autovacuum_min_duration log_connections log_disconnections log_line_prefix log_min_duration_statement log_min_error_statement log_min_messages log_statement random_page_cost  
/home/db_main_node/postgresql.conf | 36 |  autovacuum_max_workers autovacuum_vacuum_cost_delay bgwriter_delay bgwriter_flush_after bgwriter_lru_maxpages bgwriter_lru_multiplier checkpoint_completion_target checkpoint_timeout DateStyle default_text_search_config dynamic_shared_memory_type lc_messages lc_monetary lc_numeric lc_time log_destination log_directory log_duration log_filename logging_collector log_rotation_age log_rotation_size log_timezone log_truncate_on_rotation maintenance_work_mem max_connections max_wal_senders max_wal_size min_wal_size shared_buffers shared_preload_libraries synchronous_commit TimeZone vacuum_cost_limit wal_level work_mem  
default | 225 | 






## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_D004"></a>
[Table of contents](#postgres-checkup_top)
# D004 pg_stat_statements and pg_stat_kcache Settings #

## Observations ##
Data collected: 2019-05-09 23:21:11 +0300 MSK  



### Master (`127.0.0.1`) ###

#### `pg_stat_statements` extension settings ####
Setting | Value | Unit | Type | Min value | Max value
--------|-------|------|------|-----------|-----------
[pg_stat_statements.max](https://postgresqlco.nf/en/doc/param/pg_stat_statements.max)|5000||integer|100 |2147483647 
[pg_stat_statements.save](https://postgresqlco.nf/en/doc/param/pg_stat_statements.save)|on||bool||
[pg_stat_statements.track](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track)|top||enum||
[pg_stat_statements.track_utility](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track_utility)|on||bool||


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F001"></a>
[Table of contents](#postgres-checkup_top)
# F001 Autovacuum: Current Settings #

## Observations ##
Data collected: 2019-05-09 23:21:11 +0300 MSK  



### Master (`127.0.0.1`) ###
&#9660;&nbsp;Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[autovacuum](https://postgresqlco.nf/en/doc/param/autovacuum)|on|<no value> | 
[autovacuum_analyze_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_scale_factor)|0.1|<no value> | 
[autovacuum_analyze_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_threshold)|50|<no value> | 
[autovacuum_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_freeze_max_age)|1200000000|<no value> | 
[autovacuum_max_workers](https://postgresqlco.nf/en/doc/param/autovacuum_max_workers)|6|<no value> | 
[autovacuum_multixact_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_multixact_freeze_max_age)|400000000|<no value> | 
[autovacuum_naptime](https://postgresqlco.nf/en/doc/param/autovacuum_naptime)|60|s | 
[autovacuum_vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_delay)|10|ms | 
[autovacuum_vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_limit)|5000|<no value> | 
[autovacuum_vacuum_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_scale_factor)|0.2|<no value> | 
[autovacuum_vacuum_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_threshold)|50|<no value> | 
[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem)|-1|kB | 
[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem)|270336|kB | 264.00 MiB
[vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/vacuum_cost_delay)|0|ms | 
[vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/vacuum_cost_limit)|4000|<no value> | 
[vacuum_cost_page_dirty](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_dirty)|20|<no value> | 
[vacuum_cost_page_hit](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_hit)|1|<no value> | 
[vacuum_cost_page_miss](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_miss)|10|<no value> | 
[vacuum_defer_cleanup_age](https://postgresqlco.nf/en/doc/param/vacuum_defer_cleanup_age)|0|<no value> | 
[vacuum_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_min_age)|50000000|<no value> | 
[vacuum_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_table_age)|150000000|<no value> | 
[vacuum_multixact_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_min_age)|5000000|<no value> | 
[vacuum_multixact_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_table_age)|150000000|<no value> | 


#### Tuned tables ####

&#9660;&nbsp;Namespace | Relation | Options
----------|----------|------
public |pgbench_accounts | fillfactor=100<br/> autovacuum_enabled=true<br/> autovacuum_freeze_max_age=1200000000<br/> autovacuum_vacuum_scale_factor=0.001<br/> autovacuum_vacuum_threshold=100<br/> autovacuum_vacuum_cost_limit=1<br/>






## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F002"></a>
[Table of contents](#postgres-checkup_top)
# F002 Autovacuum: Transaction Wraparound Check #

## Observations ##
Data collected: 2019-05-09 23:21:11 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###

#### Databases ####
  

\# | Database | &#9660;&nbsp;Age | Capacity used, % | Warning | datfrozenxid
--|--------|-----|------------------|---------|--------------
1 |postgres |1632968 |0.08 |  |1941638
2 |template1 |1638754 |0.08 |  |1935852
3 |template0 |1638754 |0.08 |  |1935852
4 |test_locks |1638768 |0.08 |  |1935838



#### Tables in the observed database ####
  

\# | Relation | Age | &#9660;&nbsp;Capacity used, % | Warning |rel_relfrozenxid | toast_relfrozenxid 
---|-------|-----|------------------|---------|-----------------|--------------------
1 |pgbench_history |1638768 |0.08 |  |1935838 |0 |
2 |pgbench_tellers |1612622 |0.08 |  |1961984 |0 |
3 |pgbench_branches |1612621 |0.08 |  |1961985 |0 |
4 |pg_catalog.pg_class |1613424 |0.08 |  |1961182 |0 |
5 |pgbench_accounts<sup>*</sup> |901147 |0.05 |  |2673459 |0 |


<sup>*</sup> This table has specific autovacuum settings. See 'F001 Autovacuum: Current settings'


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F003"></a>
[Table of contents](#postgres-checkup_top)
# F003 Autovacuum: Dead Tuples #

## Observations ##
Data collected: 2019-05-09 23:21:12 +0300 MSK  
Current database: test_locks  
Stats reset: 2 days 23:59:00 ago (2019-05-06 23:22:05 +0300 MSK)  
### Master (`127.0.0.1`) ###
  
  
\#|  Relation | reltype | Since last autovacuum | Since last vacuum | Autovacuum Count | Vacuum Count | n_tup_ins | n_tup_upd | n_tup_del | pg_class.reltuples | n_live_tup | n_dead_tup | &#9660;Dead Tuples Level, %
---|-------|------|-----------------------|-------------------|----------|---------|-----------|-----------|-----------|--------------------|------------|------------|-----------
1 |pgbench_accounts<sup>*</sup> |r |2 days 08:44:10.8782 |2 days 02:59:47.853819 |1 |6 |50000000 |1638572 |0 |50000048 |50000050 |6954 | 0.01 
2 |pgbench_history |r |<no value> |2 days 23:56:49.092562 |0 |1 |1638566 |0 |0 |1571062 |1638462 |1 | 0 

<sup>*</sup> This table has specific autovacuum settings. See 'F001 Autovacuum: Current settings'


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F004"></a>
[Table of contents](#postgres-checkup_top)
# F004 Autovacuum: Heap Bloat (Estimated) #
:warning: This report is based on estimations. The errors in bloat estimates may be significant (in some cases, up to 15% and even more). Use it only as an indicator of potential issues.

## Observations ##
Data collected: 2019-05-09 23:21:12 +0300 MSK  
Current database: test_locks  


### Master (`127.0.0.1`) ###




\# | Table | Size | Extra | &#9660;&nbsp;Estimated bloat | Est. bloat, bytes | Est. bloat factor | Est. bloat level, % | Live Data Size | Last vacuum | Fillfactor
---|-------|------|-------|------------------------------|-------------------|------------------|---------------------|----------------|-------------|------------
&nbsp;|===== TOTAL ===== |6.41&nbsp;GiB ||56.00&nbsp;KiB |57,344 |0.10 |0.00|~61.73&nbsp;GiB |||
1 |pgbench_history |78.20&nbsp;MiB |~56.00&nbsp;KiB (0.07%)|56.00&nbsp;KiB |57,344 |1.00 |0.07 |~78.15&nbsp;MiB | 2019-05-06 23:24:23  |100
2 |pgbench_branches |184.00&nbsp;KiB |~160.00&nbsp;KiB (86.96%)| | |1.00 |0.00 |~184.00&nbsp;KiB | 2019-05-09 23:20:30 (auto)  |100
3 |pgbench_tellers |480.00&nbsp;KiB |~264.00&nbsp;KiB (55.00%)| | |0.22 |0.00 |~2.11&nbsp;MiB | 2019-05-09 23:20:30 (auto)  |100
4 |pgbench_accounts<sup>*</sup> |6.33&nbsp;GiB |~162.79&nbsp;MiB (2.51%)| | |0.10 |0.00 |~61.65&nbsp;GiB | 2019-05-07 20:21:25  |100
 
<sup>*</sup> This table has specific autovacuum settings. See 'F001 Autovacuum: Current settings'

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F005"></a>
[Table of contents](#postgres-checkup_top)
# F005 Autovacuum: Index Bloat (Estimated) #
:warning: This report is based on estimations. The errors in bloat estimates may be significant (in some cases, up to 15% and even more). Use it only as an indicator of potential issues.

## Observations ##
Data collected: 2019-05-09 23:21:12 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Index (Table) | Table Size |Index Size | Extra | &#9660;&nbsp;Estimated bloat | Est. bloat, bytes | Est. bloat level, % | Live Data Size | Fillfactor
---|---------------|------------|-----------|-------|------------------------------|-------------------|---------------------|----------------|-------------
&nbsp;|===== TOTAL ===== |6.33&nbsp;GiB |1.05&nbsp;GiB ||3.98&nbsp;MiB |4,169,728|0.37||
1 |pgbench_accounts_pkey (pgbench_accounts<sup>*</sup>) |6.33&nbsp;GiB |1.05&nbsp;GiB |~111.31&nbsp;MiB (10.39%) |3.79&nbsp;MiB |3,973,120 |0.35 |~1.05&nbsp;GiB |90
2 |pgbench_tellers_pkey (pgbench_tellers) |512.00&nbsp;KiB |232.00&nbsp;KiB |~120.00&nbsp;KiB (51.72%) |112.00&nbsp;KiB |114,688 | **48.28** |~120.00&nbsp;KiB |90
3 |pgbench_branches_pkey (pgbench_branches) |216.00&nbsp;KiB |104.00&nbsp;KiB |~80.00&nbsp;KiB (76.92%) |80.00&nbsp;KiB |81,920 | **76.92** |~24.00&nbsp;KiB |90

<sup>*</sup> This table has specific autovacuum settings. See 'F001 Autovacuum: Current settings'

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F008"></a>
[Table of contents](#postgres-checkup_top)
# F008 Autovacuum: Resource Usage #

## Observations ##
Data collected: 2019-05-09 23:21:13 +0300 MSK  
### Settings ###




Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[autovacuum_max_workers](https://postgresqlco.nf/en/doc/param/autovacuum_max_workers)|6|<no value> | 
[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem)|-1|kB | 
[log_autovacuum_min_duration](https://postgresqlco.nf/en/doc/param/log_autovacuum_min_duration)|1000|ms | 
[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem)|270336|kB | 264.00 MiB
[max_connections](https://postgresqlco.nf/en/doc/param/max_connections)|100|<no value> | 
[shared_buffers](https://postgresqlco.nf/en/doc/param/shared_buffers)|102400|8kB | 800.00 MiB
[work_mem](https://postgresqlco.nf/en/doc/param/work_mem)|153600|kB | 150.00 MiB


### CPU ###

Cpu count you can see in report A001  

### RAM ###

Ram amount you can see in report A001

Max workers memory: 2&nbsp;GiB


### DISK ###

:warning: Warning: collection of current impact on disks is not yet implemented. Please refer to Postgres logs and see current read and write IO bandwidth caused by autovacuum.

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_G001"></a>
[Table of contents](#postgres-checkup_top)
# G001 Memory-related Settings #

## Observations ##
Data collected: 2019-05-09 23:21:13 +0300 MSK  



### Master (`127.0.0.1`) ###

Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem) | -1| kB | 
[effective_cache_size](https://postgresqlco.nf/en/doc/param/effective_cache_size) | 524288| 8kB | 4.00 GiB
[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem) | 270336| kB | 264.00 MiB
[max_connections](https://postgresqlco.nf/en/doc/param/max_connections) | 100| <no value> | 
[shared_buffers](https://postgresqlco.nf/en/doc/param/shared_buffers) | 102400| 8kB | 800.00 MiB
[temp_buffers](https://postgresqlco.nf/en/doc/param/temp_buffers) | 1024| 8kB | 8.00 MiB
[work_mem](https://postgresqlco.nf/en/doc/param/work_mem) | 153600| kB | 150.00 MiB






## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_G002"></a>
[Table of contents](#postgres-checkup_top)
# G002 Connections and Current Activity #

## Observations ##
Data collected: 2019-05-09 23:21:13 +0300 MSK  



### Master (`127.0.0.1`) ###
  

\# | User | DB | Current state | Count | State changed >1m ago | State changed >1h ago
----|------|----|---------------|-------|-----------------------|-----------------------
1 | ALL users | ALL databases | ALL states | 10 | 3 | 0
2 | ALL users | ALL databases | ALL states | 4 | 0 | 0
3 | ALL users | ALL databases | ALL states | 4 | 0 | 0
4 | postgres | ALL databases | active | 2 | 1 | 0
5 | postgres | ALL databases | ALL states | 1 | 0 | 0
6 | postgres | ALL databases | ALL states | 1 | 0 | 0
7 | postgres | ALL databases | idle | 3 | 2 | 0
8 | postgres | postgres | idle | 1 | 1 | 0
9 | postgres | test_locks | active | 2 | 1 | 0
10 | postgres | test_locks | idle | 2 | 1 | 0





## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_G003"></a>
[Table of contents](#postgres-checkup_top)
# G003 Timeouts, Locks, Deadlocks #

## Observations ##
Data collected: 2019-05-09 23:21:13 +0300 MSK  



### Master (`127.0.0.1`) ###
#### Timeouts ####
Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[authentication_timeout](https://postgresqlco.nf/en/doc/param/authentication_timeout)|60|s|
[idle_in_transaction_session_timeout](https://postgresqlco.nf/en/doc/param/idle_in_transaction_session_timeout)|0|ms|
[statement_timeout](https://postgresqlco.nf/en/doc/param/statement_timeout)|30000|ms|

#### Locks ####
Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[deadlock_timeout](https://postgresqlco.nf/en/doc/param/deadlock_timeout)|1000|ms|
[lock_timeout](https://postgresqlco.nf/en/doc/param/lock_timeout)|0|ms|
[max_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_locks_per_transaction)|64|<no value>|
[max_pred_locks_per_page](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_page)|2|<no value>|
[max_pred_locks_per_relation](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_relation)|-2|<no value>|
[max_pred_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_transaction)|64|<no value>|


#### User specified settings ####
User | Setting
---------|---------
checkup_test_user | [lock_timeout=3s]

#### Databases data ####
  

\# | Database | Conflicts | &#9660;&nbsp;Deadlocks | Stats reset at | Stat reset
--|-----------|-------|-----------|----------------|------------
1|postgres|0|0|2019-03-21T03:00:46.006857+03:00|49 days 20:20:28
2|test_locks|0|0|2019-05-06T23:22:04.995478+03:00|2 days 23:59:09


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_H001"></a>
[Table of contents](#postgres-checkup_top)
# H001 Invalid Indexes #

## Observations ##
Data collected: 2019-05-09 23:21:14 +0300 MSK  
Current database: test_locks  


### Master (`127.0.0.1`) ###

Invalid indexes not found


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_H002"></a>
[Table of contents](#postgres-checkup_top)
# H002 Unused and Redundant Indexes #
## Observations ##
Data collected: 2019-05-09 23:21:14 +0300 MSK  
Current database: test_locks  
Stats reset: 2 days 23:59:00 ago (2019-05-06 23:22:05 +0300 MSK)  
:warning: Statistics age is less than 30 days. Make decisions on index cleanup with caution!
### Never Used Indexes ###

Nothing found.


### Rarely Used Indexes ###

Nothing found.


### Redundant Indexes ###

Nothing found.


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_H003"></a>
[Table of contents](#postgres-checkup_top)
# H003 Non-indexed Foreign Keys #

## Observations ##
Data collected: 2019-05-09 23:21:14 +0300 MSK  
Current database: test_locks  

### Master (`127.0.0.1`) ###


No data


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_K001"></a>
[Table of contents](#postgres-checkup_top)
# K001 Globally Aggregated Query Metrics

## Observations ##
Data collected: 2019-05-09 23:21:15 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
Start: 2019-05-09T23:10:50.332261+03:00  
End: 2019-05-09T23:21:15.43394+03:00  
Period seconds: 625.10168  
Period age: 00:10:25.101679  

Error (calls): 0.00 (0.00%)  
Error (total time): 0.00 (0.00%)

Calls | Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time 
-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------
21,499<br/>34.39/sec<br/>1.00/call<br/>100.00% |28,934.63&nbsp;ms<br/>46.288ms/sec<br/>1.346ms/call<br/>100.00% |23,767<br/>38.02/sec<br/>1.11/call<br/>100.00% |128,327&nbsp;blks<br/>205.29&nbsp;blks/sec<br/>5.97&nbsp;blks/call<br/>100.00% |13,191&nbsp;blks<br/>21.10&nbsp;blks/sec<br/>0.61&nbsp;blks/call<br/>100.00% |11,927&nbsp;blks<br/>19.08&nbsp;blks/sec<br/>0.55&nbsp;blks/call<br/>100.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%





## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_K002"></a>
[Table of contents](#postgres-checkup_top)
# K002 Workload Type ("First Word" Analysis)

## Observations ##
Data collected: 2019-05-09 23:21:15 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
Start: 2019-05-09T23:10:50.332261+03:00  
End: 2019-05-09T23:21:15.43394+03:00  
Period seconds: 625.10168  
Period age: 00:10:25.101679  

Error (calls): 0.00 (0.00%)  
Error (total time): 0.00 (0.00%)

\# | Workload type | Calls | &#9660;&nbsp;Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time 
----|-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------|------- 
1 |update |20,932<br/>33.49/sec<br/>1.00/call<br/>97.36% |21,948.42&nbsp;ms<br/>35.112ms/sec<br/>1.049ms/call<br/>75.86% |20,932<br/>33.49/sec<br/>1.00/call<br/>88.07% |112,869&nbsp;blks<br/>180.56&nbsp;blks/sec<br/>5.39&nbsp;blks/call<br/>87.95% |13,191&nbsp;blks<br/>21.10&nbsp;blks/sec<br/>0.63&nbsp;blks/call<br/>100.00% |11,927&nbsp;blks<br/>19.08&nbsp;blks/sec<br/>0.57&nbsp;blks/call<br/>100.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
2 |pga4dash |567<br/>0.91/sec<br/>1.00/call<br/>2.64% |6,986.20&nbsp;ms<br/>11.176ms/sec<br/>12.321ms/call<br/>24.14% |2,835<br/>4.54/sec<br/>5.00/call<br/>11.93% |15,458&nbsp;blks<br/>24.73&nbsp;blks/sec<br/>27.26&nbsp;blks/call<br/>12.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
3 |create |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
4 |copy |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
5 |select |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
6 |vacuum |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
7 |delete |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
8 |alter |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
9 |insert |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%





## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_K003"></a>
[Table of contents](#postgres-checkup_top)
# K003 Top-50 Queries by total_time

## Observations ##
Data collected: 2019-05-09 23:21:15 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
Start: 2019-05-09T23:10:50.332261+03:00  
End: 2019-05-09T23:21:15.43394+03:00  
Period seconds: 625.10168  
Period age: 00:10:25.101679  

Error (calls): 0.00 (0.00%)  
Error (total time): 0.00 (0.00%)

The list is limited to 100 items.  

\# | Calls | &#9660;&nbsp;Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time |Query
----|-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------|-------
1 |6,978<br/>11.16/sec<br/>1.00/call<br/>32.46% |21,424.75&nbsp;ms<br/>34.274ms/sec<br/>3.070ms/call<br/>74.05% |6,978<br/>11.16/sec<br/>1.00/call<br/>29.36% |56,804&nbsp;blks<br/>90.87&nbsp;blks/sec<br/>8.14&nbsp;blks/call<br/>44.27% |13,191&nbsp;blks<br/>21.10&nbsp;blks/sec<br/>1.89&nbsp;blks/call<br/>100.00% |11,694&nbsp;blks<br/>18.71&nbsp;blks/sec<br/>1.68&nbsp;blks/call<br/>98.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |UPDATE&nbsp;pgbench\_accounts&nbsp;SET&nbsp;abalance&nbsp;=&nbsp;abalance&nbsp;+&nbsp;$1&nbsp;WHERE&nbsp;aid&nbsp;=&nbsp;$2<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/1_1.sql)
2 |567<br/>0.91/sec<br/>1.00/call<br/>2.64% |6,986.20&nbsp;ms<br/>11.176ms/sec<br/>12.321ms/call<br/>24.14% |2,835<br/>4.54/sec<br/>5.00/call<br/>11.93% |15,458&nbsp;blks<br/>24.73&nbsp;blks/sec<br/>27.26&nbsp;blks/call<br/>12.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/2_1.sql)
3 |6,977<br/>11.16/sec<br/>1.00/call<br/>32.45% |271.95&nbsp;ms<br/>0.435ms/sec<br/>0.039ms/call<br/>0.94% |6,977<br/>11.16/sec<br/>1.00/call<br/>29.36% |28,000&nbsp;blks<br/>44.79&nbsp;blks/sec<br/>4.01&nbsp;blks/call<br/>21.82% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |181&nbsp;blks<br/>0.29&nbsp;blks/sec<br/>0.03&nbsp;blks/call<br/>1.52% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |UPDATE&nbsp;pgbench\_tellers&nbsp;SET&nbsp;tbalance&nbsp;=&nbsp;tbalance&nbsp;+&nbsp;$1&nbsp;WHERE&nbsp;tid&nbsp;=&nbsp;$2<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/3_1.sql)
4 |6,977<br/>11.16/sec<br/>1.00/call<br/>32.45% |251.73&nbsp;ms<br/>0.403ms/sec<br/>0.036ms/call<br/>0.87% |6,977<br/>11.16/sec<br/>1.00/call<br/>29.36% |28,065&nbsp;blks<br/>44.90&nbsp;blks/sec<br/>4.02&nbsp;blks/call<br/>21.87% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |52&nbsp;blks<br/>0.08&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.44% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |UPDATE&nbsp;pgbench\_branches&nbsp;SET&nbsp;bbalance&nbsp;=&nbsp;bbalance&nbsp;+&nbsp;$1&nbsp;WHERE&nbsp;bid&nbsp;=&nbsp;$2<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/4_1.sql)
5 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/5_1.sql)
6 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/6_1.sql)
7 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/7_1.sql)
8 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/8_1.sql)
9 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |CREATE&nbsp;DATABASE&nbsp;my\_test\_db\_valid<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/9_1.sql)
10 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/10_1.sql)
11 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/11_1.sql)
12 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/12_1.sql)
13 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/13_1.sql)
14 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |copy&nbsp;pgbench\_accounts&nbsp;from&nbsp;stdin<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/14_1.sql)
15 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/15_1.sql)
16 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/16_1.sql)
17 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |INSERT&nbsp;INTO&nbsp;payextdata&nbsp;(payinst\_id,&nbsp;paytran\_id,&nbsp;paybatch\_id,&nbsp;attributetype,&nbsp;attributename)
&nbsp;&nbsp;SELECT&nbsp;T.payinst\_id,&nbsp;T.paytran\_id,&nbsp;T.paybatch\_id,&nbsp;$1,&nbsp;$2&nbsp;from&nbsp;(
&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$4&nbsp;+&nbsp;$5)::integer&nbsp;as&nbsp;payinst\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$6&nbsp;+&nbsp;$7)::integer&nbsp;as&nbsp;paytran\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$8&nbsp;+&nbsp;$9)::integer&nbsp;as&nbsp;paybatch\_id,&nbsp;generate\_series($10,$11)
&nbsp;&nbsp;)&nbsp;T<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/17_1.sql)
18 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/18_1.sql)
19 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |INSERT&nbsp;INTO&nbsp;payextdata&nbsp;(payinst\_id,&nbsp;paytran\_id,&nbsp;paybatch\_id,&nbsp;attributetype,&nbsp;attributename)
&nbsp;&nbsp;SELECT&nbsp;T.payinst\_id,&nbsp;T.paytran\_id,&nbsp;T.paybatch\_id,&nbsp;$1,&nbsp;$2&nbsp;from&nbsp;(
&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$4&nbsp;+&nbsp;$5)::integer&nbsp;as&nbsp;payinst\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$6&nbsp;+&nbsp;$7)::integer&nbsp;as&nbsp;paytran\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$8&nbsp;+&nbsp;$9)::integer&nbsp;as&nbsp;paybatch\_id,&nbsp;generate\_series($10,$11)
&nbsp;&nbsp;)&nbsp;T<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/19_1.sql)
20 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/20_1.sql)
21 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/21_1.sql)
22 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/22_1.sql)
23 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/23_1.sql)
24 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/24_1.sql)
25 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/25_1.sql)
26 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/26_1.sql)
27 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/27_1.sql)
28 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/28_1.sql)
29 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/29_1.sql)
30 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/30_1.sql)
31 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/31_1.sql)
32 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/32_1.sql)
33 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |vacuum&nbsp;freeze&nbsp;verbose&nbsp;pgbench\_accounts<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/33_1.sql)
34 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/34_1.sql)
35 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/35_1.sql)
36 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/36_1.sql)
37 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/37_1.sql)
38 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/38_1.sql)
39 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/39_1.sql)
40 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/40_1.sql)
41 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity)&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$2)&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3)&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$4&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$5&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$6&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Hits"&nbsp;)&nbsp;t<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/41_1.sql)
42 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |vacuum&nbsp;analyze&nbsp;pgbench\_accounts<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/42_1.sql)
43 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/43_1.sql)
44 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/44_1.sql)
45 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/45_1.sql)
46 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/46_1.sql)
47 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |DELETE&nbsp;FROM&nbsp;ordiadjust&nbsp;WHERE&nbsp;ordadjust\_id&nbsp;in&nbsp;(&nbsp;&nbsp;SELECT&nbsp;T.ordadjust\_id&nbsp;from(&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$1&nbsp;+&nbsp;$2)::integer&nbsp;as&nbsp;ordadjust\_id,&nbsp;generate\_series($3,$4)&nbsp;	)&nbsp;T&nbsp;)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/47_1.sql)
48 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/48_1.sql)
49 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/49_1.sql)
50 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/50_1.sql)
51 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/51_1.sql)
52 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/52_1.sql)
53 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |INSERT&nbsp;INTO&nbsp;payextdata&nbsp;(payinst\_id,&nbsp;paytran\_id,&nbsp;paybatch\_id,&nbsp;attributetype,&nbsp;attributename)
&nbsp;&nbsp;SELECT&nbsp;T.payinst\_id,&nbsp;T.paytran\_id,&nbsp;T.paybatch\_id,&nbsp;$1,&nbsp;$2&nbsp;from&nbsp;(
&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$4&nbsp;+&nbsp;$5)::integer&nbsp;as&nbsp;payinst\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$6&nbsp;+&nbsp;$7)::integer&nbsp;as&nbsp;paytran\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$8&nbsp;+&nbsp;$9)::integer&nbsp;as&nbsp;paybatch\_id,&nbsp;generate\_series($10,$11)
&nbsp;&nbsp;)&nbsp;T<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/53_1.sql)
54 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/54_1.sql)
55 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/55_1.sql)
56 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/56_1.sql)
57 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/57_1.sql)
58 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/58_1.sql)
59 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/59_1.sql)
60 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/60_1.sql)
61 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/61_1.sql)
62 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/62_1.sql)
63 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |DELETE&nbsp;FROM&nbsp;ordiadjust&nbsp;WHERE&nbsp;ordadjust\_id&nbsp;in&nbsp;(&nbsp;&nbsp;SELECT&nbsp;T.ordadjust\_id&nbsp;from(&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$1&nbsp;+&nbsp;$2)::integer&nbsp;as&nbsp;ordadjust\_id,&nbsp;generate\_series($3,$4)&nbsp;	)&nbsp;T&nbsp;)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/63_1.sql)
64 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/64_1.sql)
65 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/65_1.sql)
66 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/66_1.sql)
67 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/67_1.sql)
68 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/68_1.sql)
69 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/69_1.sql)
70 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/70_1.sql)
71 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/71_1.sql)
72 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/72_1.sql)
73 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/73_1.sql)
74 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/74_1.sql)
75 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/75_1.sql)
76 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/76_1.sql)
77 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/77_1.sql)
78 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |UPDATE&nbsp;ordiadjust&nbsp;SET&nbsp;descr&nbsp;=&nbsp;$1&nbsp;\|\|&nbsp;$2&nbsp;WHERE&nbsp;ordadjust\_id&nbsp;in&nbsp;(&nbsp;&nbsp;SELECT&nbsp;T.ordadjust\_id&nbsp;from(&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$3&nbsp;+&nbsp;$4)::integer&nbsp;as&nbsp;ordadjust\_id,&nbsp;generate\_series($5,$6)&nbsp;&nbsp;)&nbsp;T&nbsp;)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/78_1.sql)
79 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/79_1.sql)
80 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/80_1.sql)
81 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |ALTER&nbsp;TABLE&nbsp;public.pgbench\_accounts&nbsp;&nbsp;ADD&nbsp;COLUMN&nbsp;aid\_2&nbsp;integer<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/81_1.sql)
82 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/82_1.sql)
83 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/83_1.sql)
84 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/84_1.sql)
85 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |UPDATE&nbsp;ordiadjust&nbsp;SET&nbsp;descr&nbsp;=&nbsp;$1&nbsp;\|\|&nbsp;$2&nbsp;WHERE&nbsp;ordadjust\_id&nbsp;in&nbsp;(&nbsp;&nbsp;SELECT&nbsp;T.ordadjust\_id&nbsp;from(&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$3&nbsp;+&nbsp;$4)::integer&nbsp;as&nbsp;ordadjust\_id,&nbsp;generate\_series($5,$6)&nbsp;&nbsp;)&nbsp;T&nbsp;)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/85_1.sql)
86 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/86_1.sql)
87 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/87_1.sql)
88 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/88_1.sql)
89 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |alter&nbsp;table&nbsp;pgbench\_accounts&nbsp;add&nbsp;primary&nbsp;key&nbsp;(aid)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/89_1.sql)
90 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/90_1.sql)
91 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/91_1.sql)
92 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/92_1.sql)
93 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/93_1.sql)
94 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |vacuum&nbsp;verbose&nbsp;public.pgbench\_accounts<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/94_1.sql)
95 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/95_1.sql)
96 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/96_1.sql)
97 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |CREATE&nbsp;DATABASE&nbsp;my\_test\_db<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/97_1.sql)
98 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |ALTER&nbsp;TABLE&nbsp;public.pgbench\_accounts&nbsp;&nbsp;add&nbsp;COLUMN&nbsp;aid\_3&nbsp;integer<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/98_1.sql)
99 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/99_1.sql)
100 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/100_1.sql)





## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


# PostgreSQL Checkup. Project: 'test_2'. Database: 'test_locks'
## Epoch number: '8'
NOTICE: while most reports describe the “current database”, some of them may contain cluster-wide information describing all databases in the cluster.

Last modified at:  2019-05-09 23:21:40 +0300


<a name="postgres-checkup_top"></a>
### Table of contents ###

[A002 Version Information](#postgres-checkup_A002)  
[A003 Postgres Settings](#postgres-checkup_A003)  
[A004 Cluster Information](#postgres-checkup_A004)  
[A005 Extensions](#postgres-checkup_A005)  
[A006 Postgres Setting Deviations](#postgres-checkup_A006)  
[A007 Altered Settings](#postgres-checkup_A007)  
[D004 pg_stat_statements and pg_stat_kcache Settings](#postgres-checkup_D004)  
[F001 Autovacuum: Current Settings](#postgres-checkup_F001)  
[F002 Autovacuum: Transaction Wraparound Check](#postgres-checkup_F002)  
[F003 Autovacuum: Dead Tuples](#postgres-checkup_F003)  
[F004 Autovacuum: Heap Bloat (Estimated)](#postgres-checkup_F004)  
[F005 Autovacuum: Index Bloat (Estimated)](#postgres-checkup_F005)  
[F008 Autovacuum: Resource Usage](#postgres-checkup_F008)  
[G001 Memory-related Settings](#postgres-checkup_G001)  
[G002 Connections and Current Activity](#postgres-checkup_G002)  
[G003 Timeouts, Locks, Deadlocks](#postgres-checkup_G003)  
[H001 Invalid Indexes](#postgres-checkup_H001)  
[H002 Unused and Redundant Indexes](#postgres-checkup_H002)  
[H003 Non-indexed Foreign Keys](#postgres-checkup_H003)  
[K001 Globally Aggregated Query Metrics](#postgres-checkup_K001)  
[K002 Workload Type ("First Word" Analysis)](#postgres-checkup_K002)  
[K003 Top-50 Queries by total_time](#postgres-checkup_K003)  
[L001 Table Sizes](#postgres-checkup_L001)  
[L003 Integer (int2, int4) Out-of-range Risks in PKs](#postgres-checkup_L003)  

---
<a name="postgres-checkup_A002"></a>
[Table of contents](#postgres-checkup_top)
# A002 Version Information #

## Observations ##
Data collected: 2019-05-09 23:21:02 +0300 MSK  



### Master (`127.0.0.1`) ###

```
PostgreSQL 10.6 on x86_64-pc-linux-gnu, compiled by gcc (GCC) 4.8.5 20150623 (Red Hat 4.8.5-28), 64-bit
```






## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A003"></a>
[Table of contents](#postgres-checkup_top)
# A003 Postgres Settings #

## Observations ##
Data collected: 2019-05-09 23:21:02 +0300 MSK  



### Master (`127.0.0.1`) ###  
&#9660;&nbsp;Category | Setting | Value | Unit | Pretty value
---------|---------|-------|------|--------------
Autovacuum|[autovacuum](https://postgresqlco.nf/en/doc/param/autovacuum) | on |  | 
Autovacuum|[autovacuum_analyze_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_scale_factor) | 0.1 |  | 
Autovacuum|[autovacuum_analyze_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_threshold) | 50 |  | 
Autovacuum|[autovacuum_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_freeze_max_age) | 1200000000 |  | 
Autovacuum|[autovacuum_max_workers](https://postgresqlco.nf/en/doc/param/autovacuum_max_workers) | 6 |  | 
Autovacuum|[autovacuum_multixact_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_multixact_freeze_max_age) | 400000000 |  | 
Autovacuum|[autovacuum_naptime](https://postgresqlco.nf/en/doc/param/autovacuum_naptime) | 60 | s  | 
Autovacuum|[autovacuum_vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_delay) | 10 | ms  | 
Autovacuum|[autovacuum_vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_limit) | 5000 |  | 
Autovacuum|[autovacuum_vacuum_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_scale_factor) | 0.2 |  | 
Autovacuum|[autovacuum_vacuum_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_threshold) | 50 |  | 
Client Connection Defaults / Locale and Formatting|[client_encoding](https://postgresqlco.nf/en/doc/param/client_encoding) | UTF8 |  | 
Client Connection Defaults / Locale and Formatting|[DateStyle](https://postgresqlco.nf/en/doc/param/DateStyle) | ISO,  MDY |  | 
Client Connection Defaults / Locale and Formatting|[default_text_search_config](https://postgresqlco.nf/en/doc/param/default_text_search_config) | pg_catalog.english |  | 
Client Connection Defaults / Locale and Formatting|[extra_float_digits](https://postgresqlco.nf/en/doc/param/extra_float_digits) | 0 |  | 
Client Connection Defaults / Locale and Formatting|[IntervalStyle](https://postgresqlco.nf/en/doc/param/IntervalStyle) | postgres |  | 
Client Connection Defaults / Locale and Formatting|[lc_collate](https://postgresqlco.nf/en/doc/param/lc_collate) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_ctype](https://postgresqlco.nf/en/doc/param/lc_ctype) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_messages](https://postgresqlco.nf/en/doc/param/lc_messages) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_monetary](https://postgresqlco.nf/en/doc/param/lc_monetary) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_numeric](https://postgresqlco.nf/en/doc/param/lc_numeric) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[lc_time](https://postgresqlco.nf/en/doc/param/lc_time) | en_US.UTF-8 |  | 
Client Connection Defaults / Locale and Formatting|[server_encoding](https://postgresqlco.nf/en/doc/param/server_encoding) | UTF8 |  | 
Client Connection Defaults / Locale and Formatting|[TimeZone](https://postgresqlco.nf/en/doc/param/TimeZone) | W-SU |  | 
Client Connection Defaults / Locale and Formatting|[timezone_abbreviations](https://postgresqlco.nf/en/doc/param/timezone_abbreviations) | Default |  | 
Client Connection Defaults / Other Defaults|[dynamic_library_path](https://postgresqlco.nf/en/doc/param/dynamic_library_path) | $libdir |  | 
Client Connection Defaults / Other Defaults|[gin_fuzzy_search_limit](https://postgresqlco.nf/en/doc/param/gin_fuzzy_search_limit) | 0 |  | 
Client Connection Defaults / Other Defaults|[tcp_keepalives_count](https://postgresqlco.nf/en/doc/param/tcp_keepalives_count) | 0 |  | 
Client Connection Defaults / Other Defaults|[tcp_keepalives_idle](https://postgresqlco.nf/en/doc/param/tcp_keepalives_idle) | 0 | s  | 
Client Connection Defaults / Other Defaults|[tcp_keepalives_interval](https://postgresqlco.nf/en/doc/param/tcp_keepalives_interval) | 0 | s  | 
Client Connection Defaults / Shared Library Preloading|[local_preload_libraries](https://postgresqlco.nf/en/doc/param/local_preload_libraries) |  |  | 
Client Connection Defaults / Shared Library Preloading|[session_preload_libraries](https://postgresqlco.nf/en/doc/param/session_preload_libraries) |  |  | 
Client Connection Defaults / Shared Library Preloading|[shared_preload_libraries](https://postgresqlco.nf/en/doc/param/shared_preload_libraries) | pg_stat_statements |  | 
Client Connection Defaults / Statement Behavior|[bytea_output](https://postgresqlco.nf/en/doc/param/bytea_output) | hex |  | 
Client Connection Defaults / Statement Behavior|[check_function_bodies](https://postgresqlco.nf/en/doc/param/check_function_bodies) | on |  | 
Client Connection Defaults / Statement Behavior|[default_tablespace](https://postgresqlco.nf/en/doc/param/default_tablespace) |  |  | 
Client Connection Defaults / Statement Behavior|[default_transaction_deferrable](https://postgresqlco.nf/en/doc/param/default_transaction_deferrable) | off |  | 
Client Connection Defaults / Statement Behavior|[default_transaction_isolation](https://postgresqlco.nf/en/doc/param/default_transaction_isolation) | read committed |  | 
Client Connection Defaults / Statement Behavior|[default_transaction_read_only](https://postgresqlco.nf/en/doc/param/default_transaction_read_only) | off |  | 
Client Connection Defaults / Statement Behavior|[gin_pending_list_limit](https://postgresqlco.nf/en/doc/param/gin_pending_list_limit) | 4096 | kB  | 4.00 MiB
Client Connection Defaults / Statement Behavior|[idle_in_transaction_session_timeout](https://postgresqlco.nf/en/doc/param/idle_in_transaction_session_timeout) | 0 | ms  | 
Client Connection Defaults / Statement Behavior|[lock_timeout](https://postgresqlco.nf/en/doc/param/lock_timeout) | 0 | ms  | 
Client Connection Defaults / Statement Behavior|[search_path](https://postgresqlco.nf/en/doc/param/search_path) | "$user",  public |  | 
Client Connection Defaults / Statement Behavior|[session_replication_role](https://postgresqlco.nf/en/doc/param/session_replication_role) | origin |  | 
Client Connection Defaults / Statement Behavior|[statement_timeout](https://postgresqlco.nf/en/doc/param/statement_timeout) | 30000 | ms  | 
Client Connection Defaults / Statement Behavior|[temp_tablespaces](https://postgresqlco.nf/en/doc/param/temp_tablespaces) |  |  | 
Client Connection Defaults / Statement Behavior|[transaction_deferrable](https://postgresqlco.nf/en/doc/param/transaction_deferrable) | off |  | 
Client Connection Defaults / Statement Behavior|[transaction_isolation](https://postgresqlco.nf/en/doc/param/transaction_isolation) | read committed |  | 
Client Connection Defaults / Statement Behavior|[transaction_read_only](https://postgresqlco.nf/en/doc/param/transaction_read_only) | off |  | 
Client Connection Defaults / Statement Behavior|[vacuum_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_min_age) | 50000000 |  | 
Client Connection Defaults / Statement Behavior|[vacuum_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_table_age) | 150000000 |  | 
Client Connection Defaults / Statement Behavior|[vacuum_multixact_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_min_age) | 5000000 |  | 
Client Connection Defaults / Statement Behavior|[vacuum_multixact_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_table_age) | 150000000 |  | 
Client Connection Defaults / Statement Behavior|[xmlbinary](https://postgresqlco.nf/en/doc/param/xmlbinary) | base64 |  | 
Client Connection Defaults / Statement Behavior|[xmloption](https://postgresqlco.nf/en/doc/param/xmloption) | content |  | 
Connections and Authentication / Connection Settings|[bonjour](https://postgresqlco.nf/en/doc/param/bonjour) | off |  | 
Connections and Authentication / Connection Settings|[bonjour_name](https://postgresqlco.nf/en/doc/param/bonjour_name) |  |  | 
Connections and Authentication / Connection Settings|[listen_addresses](https://postgresqlco.nf/en/doc/param/listen_addresses) | localhost |  | 
Connections and Authentication / Connection Settings|[max_connections](https://postgresqlco.nf/en/doc/param/max_connections) | 100 |  | 
Connections and Authentication / Connection Settings|[port](https://postgresqlco.nf/en/doc/param/port) | 5432 |  | 
Connections and Authentication / Connection Settings|[superuser_reserved_connections](https://postgresqlco.nf/en/doc/param/superuser_reserved_connections) | 3 |  | 
Connections and Authentication / Connection Settings|[unix_socket_directories](https://postgresqlco.nf/en/doc/param/unix_socket_directories) | /var/run/postgresql,  /tmp |  | 
Connections and Authentication / Connection Settings|[unix_socket_group](https://postgresqlco.nf/en/doc/param/unix_socket_group) |  |  | 
Connections and Authentication / Connection Settings|[unix_socket_permissions](https://postgresqlco.nf/en/doc/param/unix_socket_permissions) | 0777 |  | 
Connections and Authentication / Security and Authentication|[authentication_timeout](https://postgresqlco.nf/en/doc/param/authentication_timeout) | 60 | s  | 
Connections and Authentication / Security and Authentication|[db_user_namespace](https://postgresqlco.nf/en/doc/param/db_user_namespace) | off |  | 
Connections and Authentication / Security and Authentication|[krb_caseins_users](https://postgresqlco.nf/en/doc/param/krb_caseins_users) | off |  | 
Connections and Authentication / Security and Authentication|[krb_server_keyfile](https://postgresqlco.nf/en/doc/param/krb_server_keyfile) | FILE:/etc/sysconfig/pgsql/krb5.keytab |  | 
Connections and Authentication / Security and Authentication|[password_encryption](https://postgresqlco.nf/en/doc/param/password_encryption) | md5 |  | 
Connections and Authentication / Security and Authentication|[row_security](https://postgresqlco.nf/en/doc/param/row_security) | on |  | 
Connections and Authentication / Security and Authentication|[ssl](https://postgresqlco.nf/en/doc/param/ssl) | off |  | 
Connections and Authentication / Security and Authentication|[ssl_ca_file](https://postgresqlco.nf/en/doc/param/ssl_ca_file) |  |  | 
Connections and Authentication / Security and Authentication|[ssl_cert_file](https://postgresqlco.nf/en/doc/param/ssl_cert_file) | server.crt |  | 
Connections and Authentication / Security and Authentication|[ssl_ciphers](https://postgresqlco.nf/en/doc/param/ssl_ciphers) | HIGH:MEDIUM:+3DES:!aNULL |  | 
Connections and Authentication / Security and Authentication|[ssl_crl_file](https://postgresqlco.nf/en/doc/param/ssl_crl_file) |  |  | 
Connections and Authentication / Security and Authentication|[ssl_dh_params_file](https://postgresqlco.nf/en/doc/param/ssl_dh_params_file) |  |  | 
Connections and Authentication / Security and Authentication|[ssl_ecdh_curve](https://postgresqlco.nf/en/doc/param/ssl_ecdh_curve) | prime256v1 |  | 
Connections and Authentication / Security and Authentication|[ssl_key_file](https://postgresqlco.nf/en/doc/param/ssl_key_file) | server.key |  | 
Connections and Authentication / Security and Authentication|[ssl_prefer_server_ciphers](https://postgresqlco.nf/en/doc/param/ssl_prefer_server_ciphers) | on |  | 
Customized Options|[pg_stat_statements.max](https://postgresqlco.nf/en/doc/param/pg_stat_statements.max) | 5000 |  | 
Customized Options|[pg_stat_statements.save](https://postgresqlco.nf/en/doc/param/pg_stat_statements.save) | on |  | 
Customized Options|[pg_stat_statements.track](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track) | top |  | 
Customized Options|[pg_stat_statements.track_utility](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track_utility) | on |  | 
Developer Options|[allow_system_table_mods](https://postgresqlco.nf/en/doc/param/allow_system_table_mods) | off |  | 
Developer Options|[ignore_checksum_failure](https://postgresqlco.nf/en/doc/param/ignore_checksum_failure) | off |  | 
Developer Options|[ignore_system_indexes](https://postgresqlco.nf/en/doc/param/ignore_system_indexes) | off |  | 
Developer Options|[post_auth_delay](https://postgresqlco.nf/en/doc/param/post_auth_delay) | 0 | s  | 
Developer Options|[pre_auth_delay](https://postgresqlco.nf/en/doc/param/pre_auth_delay) | 0 | s  | 
Developer Options|[trace_notify](https://postgresqlco.nf/en/doc/param/trace_notify) | off |  | 
Developer Options|[trace_recovery_messages](https://postgresqlco.nf/en/doc/param/trace_recovery_messages) | log |  | 
Developer Options|[trace_sort](https://postgresqlco.nf/en/doc/param/trace_sort) | off |  | 
Developer Options|[wal_consistency_checking](https://postgresqlco.nf/en/doc/param/wal_consistency_checking) |  |  | 
Developer Options|[zero_damaged_pages](https://postgresqlco.nf/en/doc/param/zero_damaged_pages) | off |  | 
Error Handling|[exit_on_error](https://postgresqlco.nf/en/doc/param/exit_on_error) | off |  | 
Error Handling|[restart_after_crash](https://postgresqlco.nf/en/doc/param/restart_after_crash) | on |  | 
File Locations|[config_file](https://postgresqlco.nf/en/doc/param/config_file) | /home/db_main_node/postgresql.conf |  | 
File Locations|[data_directory](https://postgresqlco.nf/en/doc/param/data_directory) | /home/db_main_node |  | 
File Locations|[external_pid_file](https://postgresqlco.nf/en/doc/param/external_pid_file) |  |  | 
File Locations|[hba_file](https://postgresqlco.nf/en/doc/param/hba_file) | /home/db_main_node/pg_hba.conf |  | 
File Locations|[ident_file](https://postgresqlco.nf/en/doc/param/ident_file) | /home/db_main_node/pg_ident.conf |  | 
Lock Management|[deadlock_timeout](https://postgresqlco.nf/en/doc/param/deadlock_timeout) | 1000 | ms  | 
Lock Management|[max_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_locks_per_transaction) | 64 |  | 
Lock Management|[max_pred_locks_per_page](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_page) | 2 |  | 
Lock Management|[max_pred_locks_per_relation](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_relation) | -2 |  | 
Lock Management|[max_pred_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_transaction) | 64 |  | 
Preset Options|[block_size](https://postgresqlco.nf/en/doc/param/block_size) | 8192 |  | 
Preset Options|[data_checksums](https://postgresqlco.nf/en/doc/param/data_checksums) | off |  | 
Preset Options|[debug_assertions](https://postgresqlco.nf/en/doc/param/debug_assertions) | off |  | 
Preset Options|[integer_datetimes](https://postgresqlco.nf/en/doc/param/integer_datetimes) | on |  | 
Preset Options|[max_function_args](https://postgresqlco.nf/en/doc/param/max_function_args) | 100 |  | 
Preset Options|[max_identifier_length](https://postgresqlco.nf/en/doc/param/max_identifier_length) | 63 |  | 
Preset Options|[max_index_keys](https://postgresqlco.nf/en/doc/param/max_index_keys) | 32 |  | 
Preset Options|[segment_size](https://postgresqlco.nf/en/doc/param/segment_size) | 131072 | 8kB  | 1.00 GiB
Preset Options|[server_version](https://postgresqlco.nf/en/doc/param/server_version) | 10.6 |  | 
Preset Options|[server_version_num](https://postgresqlco.nf/en/doc/param/server_version_num) | 100006 |  | 
Preset Options|[wal_block_size](https://postgresqlco.nf/en/doc/param/wal_block_size) | 8192 |  | 
Preset Options|[wal_segment_size](https://postgresqlco.nf/en/doc/param/wal_segment_size) | 2048 | 8kB  | 16.00 MiB
Process Title|[cluster_name](https://postgresqlco.nf/en/doc/param/cluster_name) |  |  | 
Process Title|[update_process_title](https://postgresqlco.nf/en/doc/param/update_process_title) | on |  | 
Query Tuning / Genetic Query Optimizer|[geqo](https://postgresqlco.nf/en/doc/param/geqo) | on |  | 
Query Tuning / Genetic Query Optimizer|[geqo_effort](https://postgresqlco.nf/en/doc/param/geqo_effort) | 5 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_generations](https://postgresqlco.nf/en/doc/param/geqo_generations) | 0 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_pool_size](https://postgresqlco.nf/en/doc/param/geqo_pool_size) | 0 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_seed](https://postgresqlco.nf/en/doc/param/geqo_seed) | 0 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_selection_bias](https://postgresqlco.nf/en/doc/param/geqo_selection_bias) | 2 |  | 
Query Tuning / Genetic Query Optimizer|[geqo_threshold](https://postgresqlco.nf/en/doc/param/geqo_threshold) | 12 |  | 
Query Tuning / Other Planner Options|[constraint_exclusion](https://postgresqlco.nf/en/doc/param/constraint_exclusion) | partition |  | 
Query Tuning / Other Planner Options|[cursor_tuple_fraction](https://postgresqlco.nf/en/doc/param/cursor_tuple_fraction) | 0.1 |  | 
Query Tuning / Other Planner Options|[default_statistics_target](https://postgresqlco.nf/en/doc/param/default_statistics_target) | 100 |  | 
Query Tuning / Other Planner Options|[force_parallel_mode](https://postgresqlco.nf/en/doc/param/force_parallel_mode) | off |  | 
Query Tuning / Other Planner Options|[from_collapse_limit](https://postgresqlco.nf/en/doc/param/from_collapse_limit) | 8 |  | 
Query Tuning / Other Planner Options|[join_collapse_limit](https://postgresqlco.nf/en/doc/param/join_collapse_limit) | 8 |  | 
Query Tuning / Planner Cost Constants|[cpu_index_tuple_cost](https://postgresqlco.nf/en/doc/param/cpu_index_tuple_cost) | 0.005 |  | 
Query Tuning / Planner Cost Constants|[cpu_operator_cost](https://postgresqlco.nf/en/doc/param/cpu_operator_cost) | 0.0025 |  | 
Query Tuning / Planner Cost Constants|[cpu_tuple_cost](https://postgresqlco.nf/en/doc/param/cpu_tuple_cost) | 0.01 |  | 
Query Tuning / Planner Cost Constants|[effective_cache_size](https://postgresqlco.nf/en/doc/param/effective_cache_size) | 524288 | 8kB  | 4.00 GiB
Query Tuning / Planner Cost Constants|[min_parallel_index_scan_size](https://postgresqlco.nf/en/doc/param/min_parallel_index_scan_size) | 64 | 8kB  | 512.00 KiB
Query Tuning / Planner Cost Constants|[min_parallel_table_scan_size](https://postgresqlco.nf/en/doc/param/min_parallel_table_scan_size) | 1024 | 8kB  | 8.00 MiB
Query Tuning / Planner Cost Constants|[parallel_setup_cost](https://postgresqlco.nf/en/doc/param/parallel_setup_cost) | 1000 |  | 
Query Tuning / Planner Cost Constants|[parallel_tuple_cost](https://postgresqlco.nf/en/doc/param/parallel_tuple_cost) | 0.1 |  | 
Query Tuning / Planner Cost Constants|[random_page_cost](https://postgresqlco.nf/en/doc/param/random_page_cost) | 2.22 |  | 
Query Tuning / Planner Cost Constants|[seq_page_cost](https://postgresqlco.nf/en/doc/param/seq_page_cost) | 1 |  | 
Query Tuning / Planner Method Configuration|[enable_bitmapscan](https://postgresqlco.nf/en/doc/param/enable_bitmapscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_gathermerge](https://postgresqlco.nf/en/doc/param/enable_gathermerge) | on |  | 
Query Tuning / Planner Method Configuration|[enable_hashagg](https://postgresqlco.nf/en/doc/param/enable_hashagg) | on |  | 
Query Tuning / Planner Method Configuration|[enable_hashjoin](https://postgresqlco.nf/en/doc/param/enable_hashjoin) | on |  | 
Query Tuning / Planner Method Configuration|[enable_indexonlyscan](https://postgresqlco.nf/en/doc/param/enable_indexonlyscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_indexscan](https://postgresqlco.nf/en/doc/param/enable_indexscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_material](https://postgresqlco.nf/en/doc/param/enable_material) | on |  | 
Query Tuning / Planner Method Configuration|[enable_mergejoin](https://postgresqlco.nf/en/doc/param/enable_mergejoin) | on |  | 
Query Tuning / Planner Method Configuration|[enable_nestloop](https://postgresqlco.nf/en/doc/param/enable_nestloop) | on |  | 
Query Tuning / Planner Method Configuration|[enable_seqscan](https://postgresqlco.nf/en/doc/param/enable_seqscan) | on |  | 
Query Tuning / Planner Method Configuration|[enable_sort](https://postgresqlco.nf/en/doc/param/enable_sort) | on |  | 
Query Tuning / Planner Method Configuration|[enable_tidscan](https://postgresqlco.nf/en/doc/param/enable_tidscan) | on |  | 
Replication|[track_commit_timestamp](https://postgresqlco.nf/en/doc/param/track_commit_timestamp) | off |  | 
Replication / Master Server|[synchronous_standby_names](https://postgresqlco.nf/en/doc/param/synchronous_standby_names) |  |  | 
Replication / Master Server|[vacuum_defer_cleanup_age](https://postgresqlco.nf/en/doc/param/vacuum_defer_cleanup_age) | 0 |  | 
Replication / Sending Servers|[max_replication_slots](https://postgresqlco.nf/en/doc/param/max_replication_slots) | 10 |  | 
Replication / Sending Servers|[max_wal_senders](https://postgresqlco.nf/en/doc/param/max_wal_senders) | 0 |  | 
Replication / Sending Servers|[wal_keep_segments](https://postgresqlco.nf/en/doc/param/wal_keep_segments) | 0 |  | 
Replication / Sending Servers|[wal_sender_timeout](https://postgresqlco.nf/en/doc/param/wal_sender_timeout) | 60000 | ms  | 
Replication / Standby Servers|[hot_standby](https://postgresqlco.nf/en/doc/param/hot_standby) | on |  | 
Replication / Standby Servers|[hot_standby_feedback](https://postgresqlco.nf/en/doc/param/hot_standby_feedback) | off |  | 
Replication / Standby Servers|[max_standby_archive_delay](https://postgresqlco.nf/en/doc/param/max_standby_archive_delay) | 30000 | ms  | 
Replication / Standby Servers|[max_standby_streaming_delay](https://postgresqlco.nf/en/doc/param/max_standby_streaming_delay) | 30000 | ms  | 
Replication / Standby Servers|[wal_receiver_status_interval](https://postgresqlco.nf/en/doc/param/wal_receiver_status_interval) | 10 | s  | 
Replication / Standby Servers|[wal_receiver_timeout](https://postgresqlco.nf/en/doc/param/wal_receiver_timeout) | 60000 | ms  | 
Replication / Standby Servers|[wal_retrieve_retry_interval](https://postgresqlco.nf/en/doc/param/wal_retrieve_retry_interval) | 5000 | ms  | 
Replication / Subscribers|[max_logical_replication_workers](https://postgresqlco.nf/en/doc/param/max_logical_replication_workers) | 4 |  | 
Replication / Subscribers|[max_sync_workers_per_subscription](https://postgresqlco.nf/en/doc/param/max_sync_workers_per_subscription) | 2 |  | 
Reporting and Logging / What to Log|[application_name](https://postgresqlco.nf/en/doc/param/application_name) | psql |  | 
Reporting and Logging / What to Log|[debug_pretty_print](https://postgresqlco.nf/en/doc/param/debug_pretty_print) | on |  | 
Reporting and Logging / What to Log|[debug_print_parse](https://postgresqlco.nf/en/doc/param/debug_print_parse) | off |  | 
Reporting and Logging / What to Log|[debug_print_plan](https://postgresqlco.nf/en/doc/param/debug_print_plan) | off |  | 
Reporting and Logging / What to Log|[debug_print_rewritten](https://postgresqlco.nf/en/doc/param/debug_print_rewritten) | off |  | 
Reporting and Logging / What to Log|[log_autovacuum_min_duration](https://postgresqlco.nf/en/doc/param/log_autovacuum_min_duration) | 1000 | ms  | 
Reporting and Logging / What to Log|[log_checkpoints](https://postgresqlco.nf/en/doc/param/log_checkpoints) | off |  | 
Reporting and Logging / What to Log|[log_connections](https://postgresqlco.nf/en/doc/param/log_connections) | on |  | 
Reporting and Logging / What to Log|[log_disconnections](https://postgresqlco.nf/en/doc/param/log_disconnections) | on |  | 
Reporting and Logging / What to Log|[log_duration](https://postgresqlco.nf/en/doc/param/log_duration) | off |  | 
Reporting and Logging / What to Log|[log_error_verbosity](https://postgresqlco.nf/en/doc/param/log_error_verbosity) | default |  | 
Reporting and Logging / What to Log|[log_hostname](https://postgresqlco.nf/en/doc/param/log_hostname) | off |  | 
Reporting and Logging / What to Log|[log_line_prefix](https://postgresqlco.nf/en/doc/param/log_line_prefix) | %m|%u|%d|%c| |  | 
Reporting and Logging / What to Log|[log_lock_waits](https://postgresqlco.nf/en/doc/param/log_lock_waits) | off |  | 
Reporting and Logging / What to Log|[log_replication_commands](https://postgresqlco.nf/en/doc/param/log_replication_commands) | off |  | 
Reporting and Logging / What to Log|[log_statement](https://postgresqlco.nf/en/doc/param/log_statement) | all |  | 
Reporting and Logging / What to Log|[log_temp_files](https://postgresqlco.nf/en/doc/param/log_temp_files) | -1 | kB  | 
Reporting and Logging / What to Log|[log_timezone](https://postgresqlco.nf/en/doc/param/log_timezone) | W-SU |  | 
Reporting and Logging / When to Log|[client_min_messages](https://postgresqlco.nf/en/doc/param/client_min_messages) | notice |  | 
Reporting and Logging / When to Log|[log_min_duration_statement](https://postgresqlco.nf/en/doc/param/log_min_duration_statement) | 0 | ms  | 
Reporting and Logging / When to Log|[log_min_error_statement](https://postgresqlco.nf/en/doc/param/log_min_error_statement) | log |  | 
Reporting and Logging / When to Log|[log_min_messages](https://postgresqlco.nf/en/doc/param/log_min_messages) | error |  | 
Reporting and Logging / Where to Log|[event_source](https://postgresqlco.nf/en/doc/param/event_source) | PostgreSQL |  | 
Reporting and Logging / Where to Log|[log_destination](https://postgresqlco.nf/en/doc/param/log_destination) | stderr |  | 
Reporting and Logging / Where to Log|[log_directory](https://postgresqlco.nf/en/doc/param/log_directory) | log |  | 
Reporting and Logging / Where to Log|[log_file_mode](https://postgresqlco.nf/en/doc/param/log_file_mode) | 0600 |  | 
Reporting and Logging / Where to Log|[log_filename](https://postgresqlco.nf/en/doc/param/log_filename) | postgresql-%a.log |  | 
Reporting and Logging / Where to Log|[logging_collector](https://postgresqlco.nf/en/doc/param/logging_collector) | on |  | 
Reporting and Logging / Where to Log|[log_rotation_age](https://postgresqlco.nf/en/doc/param/log_rotation_age) | 1440 | min  | 
Reporting and Logging / Where to Log|[log_rotation_size](https://postgresqlco.nf/en/doc/param/log_rotation_size) | 0 | kB  | 0.00 bytes
Reporting and Logging / Where to Log|[log_truncate_on_rotation](https://postgresqlco.nf/en/doc/param/log_truncate_on_rotation) | on |  | 
Reporting and Logging / Where to Log|[syslog_facility](https://postgresqlco.nf/en/doc/param/syslog_facility) | local0 |  | 
Reporting and Logging / Where to Log|[syslog_ident](https://postgresqlco.nf/en/doc/param/syslog_ident) | postgres |  | 
Reporting and Logging / Where to Log|[syslog_sequence_numbers](https://postgresqlco.nf/en/doc/param/syslog_sequence_numbers) | on |  | 
Reporting and Logging / Where to Log|[syslog_split_messages](https://postgresqlco.nf/en/doc/param/syslog_split_messages) | on |  | 
Resource Usage / Asynchronous Behavior|[backend_flush_after](https://postgresqlco.nf/en/doc/param/backend_flush_after) | 0 | 8kB  | 0.00 bytes
Resource Usage / Asynchronous Behavior|[effective_io_concurrency](https://postgresqlco.nf/en/doc/param/effective_io_concurrency) | 1 |  | 
Resource Usage / Asynchronous Behavior|[max_parallel_workers](https://postgresqlco.nf/en/doc/param/max_parallel_workers) | 8 |  | 
Resource Usage / Asynchronous Behavior|[max_parallel_workers_per_gather](https://postgresqlco.nf/en/doc/param/max_parallel_workers_per_gather) | 2 |  | 
Resource Usage / Asynchronous Behavior|[max_worker_processes](https://postgresqlco.nf/en/doc/param/max_worker_processes) | 8 |  | 
Resource Usage / Asynchronous Behavior|[old_snapshot_threshold](https://postgresqlco.nf/en/doc/param/old_snapshot_threshold) | -1 | min  | 
Resource Usage / Background Writer|[bgwriter_delay](https://postgresqlco.nf/en/doc/param/bgwriter_delay) | 100 | ms  | 
Resource Usage / Background Writer|[bgwriter_flush_after](https://postgresqlco.nf/en/doc/param/bgwriter_flush_after) | 189 | 8kB  | 1.48 MiB
Resource Usage / Background Writer|[bgwriter_lru_maxpages](https://postgresqlco.nf/en/doc/param/bgwriter_lru_maxpages) | 1000 |  | 
Resource Usage / Background Writer|[bgwriter_lru_multiplier](https://postgresqlco.nf/en/doc/param/bgwriter_lru_multiplier) | 7 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/vacuum_cost_delay) | 0 | ms  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/vacuum_cost_limit) | 4000 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_page_dirty](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_dirty) | 20 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_page_hit](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_hit) | 1 |  | 
Resource Usage / Cost-Based Vacuum Delay|[vacuum_cost_page_miss](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_miss) | 10 |  | 
Resource Usage / Disk|[temp_file_limit](https://postgresqlco.nf/en/doc/param/temp_file_limit) | -1 | kB  | 
Resource Usage / Kernel Resources|[max_files_per_process](https://postgresqlco.nf/en/doc/param/max_files_per_process) | 1000 |  | 
Resource Usage / Memory|[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem) | -1 | kB  | 
Resource Usage / Memory|[dynamic_shared_memory_type](https://postgresqlco.nf/en/doc/param/dynamic_shared_memory_type) | posix |  | 
Resource Usage / Memory|[huge_pages](https://postgresqlco.nf/en/doc/param/huge_pages) | try |  | 
Resource Usage / Memory|[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem) | 270336 | kB  | 264.00 MiB
Resource Usage / Memory|[max_prepared_transactions](https://postgresqlco.nf/en/doc/param/max_prepared_transactions) | 0 |  | 
Resource Usage / Memory|[max_stack_depth](https://postgresqlco.nf/en/doc/param/max_stack_depth) | 2048 | kB  | 2.00 MiB
Resource Usage / Memory|[replacement_sort_tuples](https://postgresqlco.nf/en/doc/param/replacement_sort_tuples) | 150000 |  | 
Resource Usage / Memory|[shared_buffers](https://postgresqlco.nf/en/doc/param/shared_buffers) | 102400 | 8kB  | 800.00 MiB
Resource Usage / Memory|[temp_buffers](https://postgresqlco.nf/en/doc/param/temp_buffers) | 1024 | 8kB  | 8.00 MiB
Resource Usage / Memory|[track_activity_query_size](https://postgresqlco.nf/en/doc/param/track_activity_query_size) | 1024 |  | 
Resource Usage / Memory|[work_mem](https://postgresqlco.nf/en/doc/param/work_mem) | 153600 | kB  | 150.00 MiB
Statistics / Monitoring|[log_executor_stats](https://postgresqlco.nf/en/doc/param/log_executor_stats) | off |  | 
Statistics / Monitoring|[log_parser_stats](https://postgresqlco.nf/en/doc/param/log_parser_stats) | off |  | 
Statistics / Monitoring|[log_planner_stats](https://postgresqlco.nf/en/doc/param/log_planner_stats) | off |  | 
Statistics / Monitoring|[log_statement_stats](https://postgresqlco.nf/en/doc/param/log_statement_stats) | off |  | 
Statistics / Query and Index Statistics Collector|[stats_temp_directory](https://postgresqlco.nf/en/doc/param/stats_temp_directory) | pg_stat_tmp |  | 
Statistics / Query and Index Statistics Collector|[track_activities](https://postgresqlco.nf/en/doc/param/track_activities) | on |  | 
Statistics / Query and Index Statistics Collector|[track_counts](https://postgresqlco.nf/en/doc/param/track_counts) | on |  | 
Statistics / Query and Index Statistics Collector|[track_functions](https://postgresqlco.nf/en/doc/param/track_functions) | none |  | 
Statistics / Query and Index Statistics Collector|[track_io_timing](https://postgresqlco.nf/en/doc/param/track_io_timing) | off |  | 
Version and Platform Compatibility / Other Platforms and Clients|[transform_null_equals](https://postgresqlco.nf/en/doc/param/transform_null_equals) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[array_nulls](https://postgresqlco.nf/en/doc/param/array_nulls) | on |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[backslash_quote](https://postgresqlco.nf/en/doc/param/backslash_quote) | safe_encoding |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[default_with_oids](https://postgresqlco.nf/en/doc/param/default_with_oids) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[escape_string_warning](https://postgresqlco.nf/en/doc/param/escape_string_warning) | on |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[lo_compat_privileges](https://postgresqlco.nf/en/doc/param/lo_compat_privileges) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[operator_precedence_warning](https://postgresqlco.nf/en/doc/param/operator_precedence_warning) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[quote_all_identifiers](https://postgresqlco.nf/en/doc/param/quote_all_identifiers) | off |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[standard_conforming_strings](https://postgresqlco.nf/en/doc/param/standard_conforming_strings) | on |  | 
Version and Platform Compatibility / Previous PostgreSQL Versions|[synchronize_seqscans](https://postgresqlco.nf/en/doc/param/synchronize_seqscans) | on |  | 
Write-Ahead Log / Archiving|[archive_command](https://postgresqlco.nf/en/doc/param/archive_command) | (disabled) |  | 
Write-Ahead Log / Archiving|[archive_mode](https://postgresqlco.nf/en/doc/param/archive_mode) | off |  | 
Write-Ahead Log / Archiving|[archive_timeout](https://postgresqlco.nf/en/doc/param/archive_timeout) | 0 | s  | 
Write-Ahead Log / Checkpoints|[checkpoint_completion_target](https://postgresqlco.nf/en/doc/param/checkpoint_completion_target) | 0.7 |  | 
Write-Ahead Log / Checkpoints|[checkpoint_flush_after](https://postgresqlco.nf/en/doc/param/checkpoint_flush_after) | 32 | 8kB  | 256.00 KiB
Write-Ahead Log / Checkpoints|[checkpoint_timeout](https://postgresqlco.nf/en/doc/param/checkpoint_timeout) | 600 | s  | 
Write-Ahead Log / Checkpoints|[checkpoint_warning](https://postgresqlco.nf/en/doc/param/checkpoint_warning) | 30 | s  | 
Write-Ahead Log / Checkpoints|[max_wal_size](https://postgresqlco.nf/en/doc/param/max_wal_size) | 1024 | MB  | 1.00 GiB
Write-Ahead Log / Checkpoints|[min_wal_size](https://postgresqlco.nf/en/doc/param/min_wal_size) | 280 | MB  | 280.00 MiB
Write-Ahead Log / Settings|[commit_delay](https://postgresqlco.nf/en/doc/param/commit_delay) | 0 |  | 
Write-Ahead Log / Settings|[commit_siblings](https://postgresqlco.nf/en/doc/param/commit_siblings) | 5 |  | 
Write-Ahead Log / Settings|[fsync](https://postgresqlco.nf/en/doc/param/fsync) | on |  | 
Write-Ahead Log / Settings|[full_page_writes](https://postgresqlco.nf/en/doc/param/full_page_writes) | on |  | 
Write-Ahead Log / Settings|[synchronous_commit](https://postgresqlco.nf/en/doc/param/synchronous_commit) | off |  | 
Write-Ahead Log / Settings|[wal_buffers](https://postgresqlco.nf/en/doc/param/wal_buffers) | 2048 | 8kB  | 16.00 MiB
Write-Ahead Log / Settings|[wal_compression](https://postgresqlco.nf/en/doc/param/wal_compression) | off |  | 
Write-Ahead Log / Settings|[wal_level](https://postgresqlco.nf/en/doc/param/wal_level) | minimal |  | 
Write-Ahead Log / Settings|[wal_log_hints](https://postgresqlco.nf/en/doc/param/wal_log_hints) | off |  | 
Write-Ahead Log / Settings|[wal_sync_method](https://postgresqlco.nf/en/doc/param/wal_sync_method) | fdatasync |  | 
Write-Ahead Log / Settings|[wal_writer_delay](https://postgresqlco.nf/en/doc/param/wal_writer_delay) | 200 | ms  | 
Write-Ahead Log / Settings|[wal_writer_flush_after](https://postgresqlco.nf/en/doc/param/wal_writer_flush_after) | 128 | 8kB  | 1.00 MiB

  
---
<a name="postgres-checkup_A004"></a>
[Table of contents](#postgres-checkup_top)
# A004 Cluster Information #

## Observations ##
Data collected: 2019-05-09 23:21:06 +0300 MSK  

&#9660;&nbsp;Indicator | 127.0.0.1 
--------|-------
Config file |/home/db_main_node/postgresql.conf
Role |Master
Replicas |
Started At |2019-05-06&nbsp;23:41:22+03
Uptime |2&nbsp;days&nbsp;23:39:45
Checkpoints |8040
Forced Checkpoints |10.3%
Checkpoint MB/sec |0.007971
Database Name |test_locks
Database Size |7639&nbsp;MB
Stats Since |2019-05-06&nbsp;23:22:05+03
Stats Age |2&nbsp;days&nbsp;23:59:01
Cache Effectiveness |77.16%
Successful Commits |100.00%
Conflicts |0
Temp Files: total size |956&nbsp;MB
Temp Files: total number of files |1
Temp Files: total number of files per day |0
Temp Files: avg file size |956&nbsp;MB
Deadlocks |0
Deadlocks per day |0


### Databases sizes ###
Database | &#9660;&nbsp;Size
---------|------
test_locks | 7.46&nbsp;GiB
postgres | 7.71&nbsp;MiB
template1 | 7.68&nbsp;MiB
template0 | 7.68&nbsp;MiB


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A005"></a>
[Table of contents](#postgres-checkup_top)
# A005 Extensions #

## Observations ##
Data collected: 2019-05-09 23:21:06 +0300 MSK  



### Master (`127.0.0.1`) ###
&#9660;&nbsp;Database | Extension name | Installed version | Default version | Is old
---------|----------------|-------------------|-----------------|--------
test_locks | pg_stat_statements | 1.6 | 1.6 | <no value>
test_locks | plpgsql | 1.0 | 1.0 | <no value>




## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A006"></a>
[Table of contents](#postgres-checkup_top)
# A006 Postgres Setting Deviations #

## Observations ##
Data collected: 2019-05-09 23:21:06 +0300 MSK  

### Settings (pg_settings) that Differ ###

No differences in `pg_settings` are found.

### Configs(pg_config) that differ ###

No differences in `pg_config` are found.



## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_A007"></a>
[Table of contents](#postgres-checkup_top)
# A007 Altered Settings #

## Observations ##
Data collected: 2019-05-09 23:21:10 +0300 MSK  



### Master (`127.0.0.1`) ###
Source | Settings specified | List of settings
-------|--------------------|-----------------
/home/db_main_node/postgresql.auto.conf | 12 |  autovacuum autovacuum_freeze_max_age autovacuum_vacuum_cost_limit log_autovacuum_min_duration log_connections log_disconnections log_line_prefix log_min_duration_statement log_min_error_statement log_min_messages log_statement random_page_cost  
/home/db_main_node/postgresql.conf | 36 |  autovacuum_max_workers autovacuum_vacuum_cost_delay bgwriter_delay bgwriter_flush_after bgwriter_lru_maxpages bgwriter_lru_multiplier checkpoint_completion_target checkpoint_timeout DateStyle default_text_search_config dynamic_shared_memory_type lc_messages lc_monetary lc_numeric lc_time log_destination log_directory log_duration log_filename logging_collector log_rotation_age log_rotation_size log_timezone log_truncate_on_rotation maintenance_work_mem max_connections max_wal_senders max_wal_size min_wal_size shared_buffers shared_preload_libraries synchronous_commit TimeZone vacuum_cost_limit wal_level work_mem  
default | 225 | 






## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_D004"></a>
[Table of contents](#postgres-checkup_top)
# D004 pg_stat_statements and pg_stat_kcache Settings #

## Observations ##
Data collected: 2019-05-09 23:21:11 +0300 MSK  



### Master (`127.0.0.1`) ###

#### `pg_stat_statements` extension settings ####
Setting | Value | Unit | Type | Min value | Max value
--------|-------|------|------|-----------|-----------
[pg_stat_statements.max](https://postgresqlco.nf/en/doc/param/pg_stat_statements.max)|5000||integer|100 |2147483647 
[pg_stat_statements.save](https://postgresqlco.nf/en/doc/param/pg_stat_statements.save)|on||bool||
[pg_stat_statements.track](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track)|top||enum||
[pg_stat_statements.track_utility](https://postgresqlco.nf/en/doc/param/pg_stat_statements.track_utility)|on||bool||


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F001"></a>
[Table of contents](#postgres-checkup_top)
# F001 Autovacuum: Current Settings #

## Observations ##
Data collected: 2019-05-09 23:21:11 +0300 MSK  



### Master (`127.0.0.1`) ###
&#9660;&nbsp;Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[autovacuum](https://postgresqlco.nf/en/doc/param/autovacuum)|on|<no value> | 
[autovacuum_analyze_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_scale_factor)|0.1|<no value> | 
[autovacuum_analyze_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_analyze_threshold)|50|<no value> | 
[autovacuum_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_freeze_max_age)|1200000000|<no value> | 
[autovacuum_max_workers](https://postgresqlco.nf/en/doc/param/autovacuum_max_workers)|6|<no value> | 
[autovacuum_multixact_freeze_max_age](https://postgresqlco.nf/en/doc/param/autovacuum_multixact_freeze_max_age)|400000000|<no value> | 
[autovacuum_naptime](https://postgresqlco.nf/en/doc/param/autovacuum_naptime)|60|s | 
[autovacuum_vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_delay)|10|ms | 
[autovacuum_vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_cost_limit)|5000|<no value> | 
[autovacuum_vacuum_scale_factor](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_scale_factor)|0.2|<no value> | 
[autovacuum_vacuum_threshold](https://postgresqlco.nf/en/doc/param/autovacuum_vacuum_threshold)|50|<no value> | 
[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem)|-1|kB | 
[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem)|270336|kB | 264.00 MiB
[vacuum_cost_delay](https://postgresqlco.nf/en/doc/param/vacuum_cost_delay)|0|ms | 
[vacuum_cost_limit](https://postgresqlco.nf/en/doc/param/vacuum_cost_limit)|4000|<no value> | 
[vacuum_cost_page_dirty](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_dirty)|20|<no value> | 
[vacuum_cost_page_hit](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_hit)|1|<no value> | 
[vacuum_cost_page_miss](https://postgresqlco.nf/en/doc/param/vacuum_cost_page_miss)|10|<no value> | 
[vacuum_defer_cleanup_age](https://postgresqlco.nf/en/doc/param/vacuum_defer_cleanup_age)|0|<no value> | 
[vacuum_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_min_age)|50000000|<no value> | 
[vacuum_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_freeze_table_age)|150000000|<no value> | 
[vacuum_multixact_freeze_min_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_min_age)|5000000|<no value> | 
[vacuum_multixact_freeze_table_age](https://postgresqlco.nf/en/doc/param/vacuum_multixact_freeze_table_age)|150000000|<no value> | 


#### Tuned tables ####

&#9660;&nbsp;Namespace | Relation | Options
----------|----------|------
public |pgbench_accounts | fillfactor=100<br/> autovacuum_enabled=true<br/> autovacuum_freeze_max_age=1200000000<br/> autovacuum_vacuum_scale_factor=0.001<br/> autovacuum_vacuum_threshold=100<br/> autovacuum_vacuum_cost_limit=1<br/>






## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F002"></a>
[Table of contents](#postgres-checkup_top)
# F002 Autovacuum: Transaction Wraparound Check #

## Observations ##
Data collected: 2019-05-09 23:21:11 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###

#### Databases ####
  

\# | Database | &#9660;&nbsp;Age | Capacity used, % | Warning | datfrozenxid
--|--------|-----|------------------|---------|--------------
1 |postgres |1632968 |0.08 |  |1941638
2 |template1 |1638754 |0.08 |  |1935852
3 |template0 |1638754 |0.08 |  |1935852
4 |test_locks |1638768 |0.08 |  |1935838



#### Tables in the observed database ####
  

\# | Relation | Age | &#9660;&nbsp;Capacity used, % | Warning |rel_relfrozenxid | toast_relfrozenxid 
---|-------|-----|------------------|---------|-----------------|--------------------
1 |pgbench_history |1638768 |0.08 |  |1935838 |0 |
2 |pgbench_tellers |1612622 |0.08 |  |1961984 |0 |
3 |pgbench_branches |1612621 |0.08 |  |1961985 |0 |
4 |pg_catalog.pg_class |1613424 |0.08 |  |1961182 |0 |
5 |pgbench_accounts<sup>*</sup> |901147 |0.05 |  |2673459 |0 |


<sup>*</sup> This table has specific autovacuum settings. See 'F001 Autovacuum: Current settings'


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F003"></a>
[Table of contents](#postgres-checkup_top)
# F003 Autovacuum: Dead Tuples #

## Observations ##
Data collected: 2019-05-09 23:21:12 +0300 MSK  
Current database: test_locks  
Stats reset: 2 days 23:59:00 ago (2019-05-06 23:22:05 +0300 MSK)  
### Master (`127.0.0.1`) ###
  
  
\#|  Relation | reltype | Since last autovacuum | Since last vacuum | Autovacuum Count | Vacuum Count | n_tup_ins | n_tup_upd | n_tup_del | pg_class.reltuples | n_live_tup | n_dead_tup | &#9660;Dead Tuples Level, %
---|-------|------|-----------------------|-------------------|----------|---------|-----------|-----------|-----------|--------------------|------------|------------|-----------
1 |pgbench_accounts<sup>*</sup> |r |2 days 08:44:10.8782 |2 days 02:59:47.853819 |1 |6 |50000000 |1638572 |0 |50000048 |50000050 |6954 | 0.01 
2 |pgbench_history |r |<no value> |2 days 23:56:49.092562 |0 |1 |1638566 |0 |0 |1571062 |1638462 |1 | 0 

<sup>*</sup> This table has specific autovacuum settings. See 'F001 Autovacuum: Current settings'


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F004"></a>
[Table of contents](#postgres-checkup_top)
# F004 Autovacuum: Heap Bloat (Estimated) #
:warning: This report is based on estimations. The errors in bloat estimates may be significant (in some cases, up to 15% and even more). Use it only as an indicator of potential issues.

## Observations ##
Data collected: 2019-05-09 23:21:12 +0300 MSK  
Current database: test_locks  


### Master (`127.0.0.1`) ###




\# | Table | Size | Extra | &#9660;&nbsp;Estimated bloat | Est. bloat, bytes | Est. bloat factor | Est. bloat level, % | Live Data Size | Last vacuum | Fillfactor
---|-------|------|-------|------------------------------|-------------------|------------------|---------------------|----------------|-------------|------------
&nbsp;|===== TOTAL ===== |6.41&nbsp;GiB ||56.00&nbsp;KiB |57,344 |0.10 |0.00|~61.73&nbsp;GiB |||
1 |pgbench_history |78.20&nbsp;MiB |~56.00&nbsp;KiB (0.07%)|56.00&nbsp;KiB |57,344 |1.00 |0.07 |~78.15&nbsp;MiB | 2019-05-06 23:24:23  |100
2 |pgbench_branches |184.00&nbsp;KiB |~160.00&nbsp;KiB (86.96%)| | |1.00 |0.00 |~184.00&nbsp;KiB | 2019-05-09 23:20:30 (auto)  |100
3 |pgbench_tellers |480.00&nbsp;KiB |~264.00&nbsp;KiB (55.00%)| | |0.22 |0.00 |~2.11&nbsp;MiB | 2019-05-09 23:20:30 (auto)  |100
4 |pgbench_accounts<sup>*</sup> |6.33&nbsp;GiB |~162.79&nbsp;MiB (2.51%)| | |0.10 |0.00 |~61.65&nbsp;GiB | 2019-05-07 20:21:25  |100
 
<sup>*</sup> This table has specific autovacuum settings. See 'F001 Autovacuum: Current settings'

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F005"></a>
[Table of contents](#postgres-checkup_top)
# F005 Autovacuum: Index Bloat (Estimated) #
:warning: This report is based on estimations. The errors in bloat estimates may be significant (in some cases, up to 15% and even more). Use it only as an indicator of potential issues.

## Observations ##
Data collected: 2019-05-09 23:21:12 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Index (Table) | Table Size |Index Size | Extra | &#9660;&nbsp;Estimated bloat | Est. bloat, bytes | Est. bloat level, % | Live Data Size | Fillfactor
---|---------------|------------|-----------|-------|------------------------------|-------------------|---------------------|----------------|-------------
&nbsp;|===== TOTAL ===== |6.33&nbsp;GiB |1.05&nbsp;GiB ||3.98&nbsp;MiB |4,169,728|0.37||
1 |pgbench_accounts_pkey (pgbench_accounts<sup>*</sup>) |6.33&nbsp;GiB |1.05&nbsp;GiB |~111.31&nbsp;MiB (10.39%) |3.79&nbsp;MiB |3,973,120 |0.35 |~1.05&nbsp;GiB |90
2 |pgbench_tellers_pkey (pgbench_tellers) |512.00&nbsp;KiB |232.00&nbsp;KiB |~120.00&nbsp;KiB (51.72%) |112.00&nbsp;KiB |114,688 | **48.28** |~120.00&nbsp;KiB |90
3 |pgbench_branches_pkey (pgbench_branches) |216.00&nbsp;KiB |104.00&nbsp;KiB |~80.00&nbsp;KiB (76.92%) |80.00&nbsp;KiB |81,920 | **76.92** |~24.00&nbsp;KiB |90

<sup>*</sup> This table has specific autovacuum settings. See 'F001 Autovacuum: Current settings'

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_F008"></a>
[Table of contents](#postgres-checkup_top)
# F008 Autovacuum: Resource Usage #

## Observations ##
Data collected: 2019-05-09 23:21:13 +0300 MSK  
### Settings ###




Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[autovacuum_max_workers](https://postgresqlco.nf/en/doc/param/autovacuum_max_workers)|6|<no value> | 
[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem)|-1|kB | 
[log_autovacuum_min_duration](https://postgresqlco.nf/en/doc/param/log_autovacuum_min_duration)|1000|ms | 
[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem)|270336|kB | 264.00 MiB
[max_connections](https://postgresqlco.nf/en/doc/param/max_connections)|100|<no value> | 
[shared_buffers](https://postgresqlco.nf/en/doc/param/shared_buffers)|102400|8kB | 800.00 MiB
[work_mem](https://postgresqlco.nf/en/doc/param/work_mem)|153600|kB | 150.00 MiB


### CPU ###

Cpu count you can see in report A001  

### RAM ###

Ram amount you can see in report A001

Max workers memory: 2&nbsp;GiB


### DISK ###

:warning: Warning: collection of current impact on disks is not yet implemented. Please refer to Postgres logs and see current read and write IO bandwidth caused by autovacuum.

## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_G001"></a>
[Table of contents](#postgres-checkup_top)
# G001 Memory-related Settings #

## Observations ##
Data collected: 2019-05-09 23:21:13 +0300 MSK  



### Master (`127.0.0.1`) ###

Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[autovacuum_work_mem](https://postgresqlco.nf/en/doc/param/autovacuum_work_mem) | -1| kB | 
[effective_cache_size](https://postgresqlco.nf/en/doc/param/effective_cache_size) | 524288| 8kB | 4.00 GiB
[maintenance_work_mem](https://postgresqlco.nf/en/doc/param/maintenance_work_mem) | 270336| kB | 264.00 MiB
[max_connections](https://postgresqlco.nf/en/doc/param/max_connections) | 100| <no value> | 
[shared_buffers](https://postgresqlco.nf/en/doc/param/shared_buffers) | 102400| 8kB | 800.00 MiB
[temp_buffers](https://postgresqlco.nf/en/doc/param/temp_buffers) | 1024| 8kB | 8.00 MiB
[work_mem](https://postgresqlco.nf/en/doc/param/work_mem) | 153600| kB | 150.00 MiB






## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_G002"></a>
[Table of contents](#postgres-checkup_top)
# G002 Connections and Current Activity #

## Observations ##
Data collected: 2019-05-09 23:21:13 +0300 MSK  



### Master (`127.0.0.1`) ###
  

\# | User | DB | Current state | Count | State changed >1m ago | State changed >1h ago
----|------|----|---------------|-------|-----------------------|-----------------------
1 | ALL users | ALL databases | ALL states | 10 | 3 | 0
2 | ALL users | ALL databases | ALL states | 4 | 0 | 0
3 | ALL users | ALL databases | ALL states | 4 | 0 | 0
4 | postgres | ALL databases | active | 2 | 1 | 0
5 | postgres | ALL databases | ALL states | 1 | 0 | 0
6 | postgres | ALL databases | ALL states | 1 | 0 | 0
7 | postgres | ALL databases | idle | 3 | 2 | 0
8 | postgres | postgres | idle | 1 | 1 | 0
9 | postgres | test_locks | active | 2 | 1 | 0
10 | postgres | test_locks | idle | 2 | 1 | 0





## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_G003"></a>
[Table of contents](#postgres-checkup_top)
# G003 Timeouts, Locks, Deadlocks #

## Observations ##
Data collected: 2019-05-09 23:21:13 +0300 MSK  



### Master (`127.0.0.1`) ###
#### Timeouts ####
Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[authentication_timeout](https://postgresqlco.nf/en/doc/param/authentication_timeout)|60|s|
[idle_in_transaction_session_timeout](https://postgresqlco.nf/en/doc/param/idle_in_transaction_session_timeout)|0|ms|
[statement_timeout](https://postgresqlco.nf/en/doc/param/statement_timeout)|30000|ms|

#### Locks ####
Setting name | Value | Unit | Pretty value
-------------|-------|------|--------------
[deadlock_timeout](https://postgresqlco.nf/en/doc/param/deadlock_timeout)|1000|ms|
[lock_timeout](https://postgresqlco.nf/en/doc/param/lock_timeout)|0|ms|
[max_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_locks_per_transaction)|64|<no value>|
[max_pred_locks_per_page](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_page)|2|<no value>|
[max_pred_locks_per_relation](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_relation)|-2|<no value>|
[max_pred_locks_per_transaction](https://postgresqlco.nf/en/doc/param/max_pred_locks_per_transaction)|64|<no value>|


#### User specified settings ####
User | Setting
---------|---------
checkup_test_user | [lock_timeout=3s]

#### Databases data ####
  

\# | Database | Conflicts | &#9660;&nbsp;Deadlocks | Stats reset at | Stat reset
--|-----------|-------|-----------|----------------|------------
1|postgres|0|0|2019-03-21T03:00:46.006857+03:00|49 days 20:20:28
2|test_locks|0|0|2019-05-06T23:22:04.995478+03:00|2 days 23:59:09


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_H001"></a>
[Table of contents](#postgres-checkup_top)
# H001 Invalid Indexes #

## Observations ##
Data collected: 2019-05-09 23:21:14 +0300 MSK  
Current database: test_locks  


### Master (`127.0.0.1`) ###

Invalid indexes not found


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_H002"></a>
[Table of contents](#postgres-checkup_top)
# H002 Unused and Redundant Indexes #
## Observations ##
Data collected: 2019-05-09 23:21:14 +0300 MSK  
Current database: test_locks  
Stats reset: 2 days 23:59:00 ago (2019-05-06 23:22:05 +0300 MSK)  
:warning: Statistics age is less than 30 days. Make decisions on index cleanup with caution!
### Never Used Indexes ###

Nothing found.


### Rarely Used Indexes ###

Nothing found.


### Redundant Indexes ###

Nothing found.


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_H003"></a>
[Table of contents](#postgres-checkup_top)
# H003 Non-indexed Foreign Keys #

## Observations ##
Data collected: 2019-05-09 23:21:14 +0300 MSK  
Current database: test_locks  

### Master (`127.0.0.1`) ###


No data


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_K001"></a>
[Table of contents](#postgres-checkup_top)
# K001 Globally Aggregated Query Metrics

## Observations ##
Data collected: 2019-05-09 23:21:15 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
Start: 2019-05-09T23:10:50.332261+03:00  
End: 2019-05-09T23:21:15.43394+03:00  
Period seconds: 625.10168  
Period age: 00:10:25.101679  

Error (calls): 0.00 (0.00%)  
Error (total time): 0.00 (0.00%)

Calls | Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time 
-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------
21,499<br/>34.39/sec<br/>1.00/call<br/>100.00% |28,934.63&nbsp;ms<br/>46.288ms/sec<br/>1.346ms/call<br/>100.00% |23,767<br/>38.02/sec<br/>1.11/call<br/>100.00% |128,327&nbsp;blks<br/>205.29&nbsp;blks/sec<br/>5.97&nbsp;blks/call<br/>100.00% |13,191&nbsp;blks<br/>21.10&nbsp;blks/sec<br/>0.61&nbsp;blks/call<br/>100.00% |11,927&nbsp;blks<br/>19.08&nbsp;blks/sec<br/>0.55&nbsp;blks/call<br/>100.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%





## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_K002"></a>
[Table of contents](#postgres-checkup_top)
# K002 Workload Type ("First Word" Analysis)

## Observations ##
Data collected: 2019-05-09 23:21:15 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
Start: 2019-05-09T23:10:50.332261+03:00  
End: 2019-05-09T23:21:15.43394+03:00  
Period seconds: 625.10168  
Period age: 00:10:25.101679  

Error (calls): 0.00 (0.00%)  
Error (total time): 0.00 (0.00%)

\# | Workload type | Calls | &#9660;&nbsp;Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time 
----|-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------|------- 
1 |update |20,932<br/>33.49/sec<br/>1.00/call<br/>97.36% |21,948.42&nbsp;ms<br/>35.112ms/sec<br/>1.049ms/call<br/>75.86% |20,932<br/>33.49/sec<br/>1.00/call<br/>88.07% |112,869&nbsp;blks<br/>180.56&nbsp;blks/sec<br/>5.39&nbsp;blks/call<br/>87.95% |13,191&nbsp;blks<br/>21.10&nbsp;blks/sec<br/>0.63&nbsp;blks/call<br/>100.00% |11,927&nbsp;blks<br/>19.08&nbsp;blks/sec<br/>0.57&nbsp;blks/call<br/>100.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
2 |pga4dash |567<br/>0.91/sec<br/>1.00/call<br/>2.64% |6,986.20&nbsp;ms<br/>11.176ms/sec<br/>12.321ms/call<br/>24.14% |2,835<br/>4.54/sec<br/>5.00/call<br/>11.93% |15,458&nbsp;blks<br/>24.73&nbsp;blks/sec<br/>27.26&nbsp;blks/call<br/>12.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
3 |create |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
4 |copy |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
5 |select |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
6 |vacuum |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
7 |delete |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
8 |alter |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%
9 |insert |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00%





## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_K003"></a>
[Table of contents](#postgres-checkup_top)
# K003 Top-50 Queries by total_time

## Observations ##
Data collected: 2019-05-09 23:21:15 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
Start: 2019-05-09T23:10:50.332261+03:00  
End: 2019-05-09T23:21:15.43394+03:00  
Period seconds: 625.10168  
Period age: 00:10:25.101679  

Error (calls): 0.00 (0.00%)  
Error (total time): 0.00 (0.00%)

The list is limited to 100 items.  

\# | Calls | &#9660;&nbsp;Total&nbsp;time | Rows | shared_blks_hit | shared_blks_read | shared_blks_dirtied | shared_blks_written | blk_read_time | blk_write_time | kcache_reads | kcache_writes | kcache_user_time_ms | kcache_system_time |Query
----|-------|------------|------|-----------------|------------------|---------------------|---------------------|---------------|----------------|--------------|---------------|---------------------|--------------------|-------
1 |6,978<br/>11.16/sec<br/>1.00/call<br/>32.46% |21,424.75&nbsp;ms<br/>34.274ms/sec<br/>3.070ms/call<br/>74.05% |6,978<br/>11.16/sec<br/>1.00/call<br/>29.36% |56,804&nbsp;blks<br/>90.87&nbsp;blks/sec<br/>8.14&nbsp;blks/call<br/>44.27% |13,191&nbsp;blks<br/>21.10&nbsp;blks/sec<br/>1.89&nbsp;blks/call<br/>100.00% |11,694&nbsp;blks<br/>18.71&nbsp;blks/sec<br/>1.68&nbsp;blks/call<br/>98.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |UPDATE&nbsp;pgbench\_accounts&nbsp;SET&nbsp;abalance&nbsp;=&nbsp;abalance&nbsp;+&nbsp;$1&nbsp;WHERE&nbsp;aid&nbsp;=&nbsp;$2<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/1_1.sql)
2 |567<br/>0.91/sec<br/>1.00/call<br/>2.64% |6,986.20&nbsp;ms<br/>11.176ms/sec<br/>12.321ms/call<br/>24.14% |2,835<br/>4.54/sec<br/>5.00/call<br/>11.93% |15,458&nbsp;blks<br/>24.73&nbsp;blks/sec<br/>27.26&nbsp;blks/call<br/>12.05% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/2_1.sql)
3 |6,977<br/>11.16/sec<br/>1.00/call<br/>32.45% |271.95&nbsp;ms<br/>0.435ms/sec<br/>0.039ms/call<br/>0.94% |6,977<br/>11.16/sec<br/>1.00/call<br/>29.36% |28,000&nbsp;blks<br/>44.79&nbsp;blks/sec<br/>4.01&nbsp;blks/call<br/>21.82% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |181&nbsp;blks<br/>0.29&nbsp;blks/sec<br/>0.03&nbsp;blks/call<br/>1.52% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |UPDATE&nbsp;pgbench\_tellers&nbsp;SET&nbsp;tbalance&nbsp;=&nbsp;tbalance&nbsp;+&nbsp;$1&nbsp;WHERE&nbsp;tid&nbsp;=&nbsp;$2<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/3_1.sql)
4 |6,977<br/>11.16/sec<br/>1.00/call<br/>32.45% |251.73&nbsp;ms<br/>0.403ms/sec<br/>0.036ms/call<br/>0.87% |6,977<br/>11.16/sec<br/>1.00/call<br/>29.36% |28,065&nbsp;blks<br/>44.90&nbsp;blks/sec<br/>4.02&nbsp;blks/call<br/>21.87% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |52&nbsp;blks<br/>0.08&nbsp;blks/sec<br/>0.01&nbsp;blks/call<br/>0.44% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |UPDATE&nbsp;pgbench\_branches&nbsp;SET&nbsp;bbalance&nbsp;=&nbsp;bbalance&nbsp;+&nbsp;$1&nbsp;WHERE&nbsp;bid&nbsp;=&nbsp;$2<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/4_1.sql)
5 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/5_1.sql)
6 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/6_1.sql)
7 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/7_1.sql)
8 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/8_1.sql)
9 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |CREATE&nbsp;DATABASE&nbsp;my\_test\_db\_valid<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/9_1.sql)
10 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/10_1.sql)
11 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/11_1.sql)
12 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/12_1.sql)
13 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/13_1.sql)
14 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |copy&nbsp;pgbench\_accounts&nbsp;from&nbsp;stdin<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/14_1.sql)
15 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/15_1.sql)
16 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/16_1.sql)
17 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |INSERT&nbsp;INTO&nbsp;payextdata&nbsp;(payinst\_id,&nbsp;paytran\_id,&nbsp;paybatch\_id,&nbsp;attributetype,&nbsp;attributename)
&nbsp;&nbsp;SELECT&nbsp;T.payinst\_id,&nbsp;T.paytran\_id,&nbsp;T.paybatch\_id,&nbsp;$1,&nbsp;$2&nbsp;from&nbsp;(
&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$4&nbsp;+&nbsp;$5)::integer&nbsp;as&nbsp;payinst\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$6&nbsp;+&nbsp;$7)::integer&nbsp;as&nbsp;paytran\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$8&nbsp;+&nbsp;$9)::integer&nbsp;as&nbsp;paybatch\_id,&nbsp;generate\_series($10,$11)
&nbsp;&nbsp;)&nbsp;T<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/17_1.sql)
18 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/18_1.sql)
19 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |INSERT&nbsp;INTO&nbsp;payextdata&nbsp;(payinst\_id,&nbsp;paytran\_id,&nbsp;paybatch\_id,&nbsp;attributetype,&nbsp;attributename)
&nbsp;&nbsp;SELECT&nbsp;T.payinst\_id,&nbsp;T.paytran\_id,&nbsp;T.paybatch\_id,&nbsp;$1,&nbsp;$2&nbsp;from&nbsp;(
&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$4&nbsp;+&nbsp;$5)::integer&nbsp;as&nbsp;payinst\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$6&nbsp;+&nbsp;$7)::integer&nbsp;as&nbsp;paytran\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$8&nbsp;+&nbsp;$9)::integer&nbsp;as&nbsp;paybatch\_id,&nbsp;generate\_series($10,$11)
&nbsp;&nbsp;)&nbsp;T<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/19_1.sql)
20 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/20_1.sql)
21 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/21_1.sql)
22 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/22_1.sql)
23 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/23_1.sql)
24 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/24_1.sql)
25 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/25_1.sql)
26 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/26_1.sql)
27 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/27_1.sql)
28 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/28_1.sql)
29 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/29_1.sql)
30 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/30_1.sql)
31 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/31_1.sql)
32 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/32_1.sql)
33 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |vacuum&nbsp;freeze&nbsp;verbose&nbsp;pgbench\_accounts<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/33_1.sql)
34 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/34_1.sql)
35 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/35_1.sql)
36 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/36_1.sql)
37 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/37_1.sql)
38 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/38_1.sql)
39 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/39_1.sql)
40 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/40_1.sql)
41 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity)&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$2)&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3)&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$4&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$5&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$6&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;FROM&nbsp;pg\_stat\_database)&nbsp;AS&nbsp;"Hits"&nbsp;)&nbsp;t<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/41_1.sql)
42 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |vacuum&nbsp;analyze&nbsp;pgbench\_accounts<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/42_1.sql)
43 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/43_1.sql)
44 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/44_1.sql)
45 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/45_1.sql)
46 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/46_1.sql)
47 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |DELETE&nbsp;FROM&nbsp;ordiadjust&nbsp;WHERE&nbsp;ordadjust\_id&nbsp;in&nbsp;(&nbsp;&nbsp;SELECT&nbsp;T.ordadjust\_id&nbsp;from(&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$1&nbsp;+&nbsp;$2)::integer&nbsp;as&nbsp;ordadjust\_id,&nbsp;generate\_series($3,$4)&nbsp;	)&nbsp;T&nbsp;)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/47_1.sql)
48 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/48_1.sql)
49 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/49_1.sql)
50 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/50_1.sql)
51 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/51_1.sql)
52 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/52_1.sql)
53 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |INSERT&nbsp;INTO&nbsp;payextdata&nbsp;(payinst\_id,&nbsp;paytran\_id,&nbsp;paybatch\_id,&nbsp;attributetype,&nbsp;attributename)
&nbsp;&nbsp;SELECT&nbsp;T.payinst\_id,&nbsp;T.paytran\_id,&nbsp;T.paybatch\_id,&nbsp;$1,&nbsp;$2&nbsp;from&nbsp;(
&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$4&nbsp;+&nbsp;$5)::integer&nbsp;as&nbsp;payinst\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$6&nbsp;+&nbsp;$7)::integer&nbsp;as&nbsp;paytran\_id,
&nbsp;&nbsp;floor(random()&nbsp;\*&nbsp;$8&nbsp;+&nbsp;$9)::integer&nbsp;as&nbsp;paybatch\_id,&nbsp;generate\_series($10,$11)
&nbsp;&nbsp;)&nbsp;T<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/53_1.sql)
54 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/54_1.sql)
55 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/55_1.sql)
56 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/56_1.sql)
57 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/57_1.sql)
58 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/58_1.sql)
59 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/59_1.sql)
60 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/60_1.sql)
61 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/61_1.sql)
62 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/62_1.sql)
63 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |DELETE&nbsp;FROM&nbsp;ordiadjust&nbsp;WHERE&nbsp;ordadjust\_id&nbsp;in&nbsp;(&nbsp;&nbsp;SELECT&nbsp;T.ordadjust\_id&nbsp;from(&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$1&nbsp;+&nbsp;$2)::integer&nbsp;as&nbsp;ordadjust\_id,&nbsp;generate\_series($3,$4)&nbsp;	)&nbsp;T&nbsp;)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/63_1.sql)
64 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/64_1.sql)
65 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/65_1.sql)
66 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/66_1.sql)
67 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/67_1.sql)
68 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/68_1.sql)
69 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/69_1.sql)
70 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/70_1.sql)
71 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/71_1.sql)
72 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/72_1.sql)
73 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/73_1.sql)
74 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/74_1.sql)
75 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/75_1.sql)
76 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/76_1.sql)
77 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/77_1.sql)
78 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |UPDATE&nbsp;ordiadjust&nbsp;SET&nbsp;descr&nbsp;=&nbsp;$1&nbsp;\|\|&nbsp;$2&nbsp;WHERE&nbsp;ordadjust\_id&nbsp;in&nbsp;(&nbsp;&nbsp;SELECT&nbsp;T.ordadjust\_id&nbsp;from(&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$3&nbsp;+&nbsp;$4)::integer&nbsp;as&nbsp;ordadjust\_id,&nbsp;generate\_series($5,$6)&nbsp;&nbsp;)&nbsp;T&nbsp;)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/78_1.sql)
79 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/79_1.sql)
80 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/80_1.sql)
81 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |ALTER&nbsp;TABLE&nbsp;public.pgbench\_accounts&nbsp;&nbsp;ADD&nbsp;COLUMN&nbsp;aid\_2&nbsp;integer<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/81_1.sql)
82 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |/\*pga4dash\*/&nbsp;SELECT&nbsp;$1&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$2))&nbsp;AS&nbsp;"Total",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$3&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$4))&nbsp;AS&nbsp;"Active",&nbsp;&nbsp;(SELECT&nbsp;count(\*)&nbsp;FROM&nbsp;pg\_stat\_activity&nbsp;WHERE&nbsp;state&nbsp;=&nbsp;$5&nbsp;AND&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$6))&nbsp;AS&nbsp;"Idle"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$7&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;+&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$8))&nbsp;AS&nbsp;"Transactions",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_commit)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$9))&nbsp;AS&nbsp;"Commits",&nbsp;&nbsp;(SELECT&nbsp;sum(xact\_rollback)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$10))&nbsp;AS&nbsp;"Rollbacks"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$11&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_inserted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$12))&nbsp;AS&nbsp;"Inserts",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_updated)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$13))&nbsp;AS&nbsp;"Updates",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_deleted)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$14))&nbsp;AS&nbsp;"Deletes"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$15&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_fetched)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$16))&nbsp;AS&nbsp;"Fetched",&nbsp;&nbsp;(SELECT&nbsp;sum(tup\_returned)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$17))&nbsp;AS&nbsp;"Returned"&nbsp;)&nbsp;t&nbsp;UNION&nbsp;ALL&nbsp;SELECT&nbsp;$18&nbsp;AS&nbsp;chart\_name,&nbsp;row\_to\_json(t)&nbsp;AS&nbsp;chart\_data&nbsp;FROM&nbsp;(SELECT&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_read)&nbsp;FROM&nbsp;pg\_stat\_database&nbsp;WHERE&nbsp;datname&nbsp;=&nbsp;(SELECT&nbsp;datname&nbsp;FROM&nbsp;pg\_database&nbsp;WHERE&nbsp;oid&nbsp;=&nbsp;$19))&nbsp;AS&nbsp;"Reads",&nbsp;&nbsp;(SELECT&nbsp;sum(blks\_hit)&nbsp;F...<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/82_1.sql)
83 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/83_1.sql)
84 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/84_1.sql)
85 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |UPDATE&nbsp;ordiadjust&nbsp;SET&nbsp;descr&nbsp;=&nbsp;$1&nbsp;\|\|&nbsp;$2&nbsp;WHERE&nbsp;ordadjust\_id&nbsp;in&nbsp;(&nbsp;&nbsp;SELECT&nbsp;T.ordadjust\_id&nbsp;from(&nbsp;&nbsp;SELECT&nbsp;floor(random()&nbsp;\*&nbsp;$3&nbsp;+&nbsp;$4)::integer&nbsp;as&nbsp;ordadjust\_id,&nbsp;generate\_series($5,$6)&nbsp;&nbsp;)&nbsp;T&nbsp;)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/85_1.sql)
86 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/86_1.sql)
87 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/87_1.sql)
88 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/88_1.sql)
89 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |alter&nbsp;table&nbsp;pgbench\_accounts&nbsp;add&nbsp;primary&nbsp;key&nbsp;(aid)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/89_1.sql)
90 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/90_1.sql)
91 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/91_1.sql)
92 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/92_1.sql)
93 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |delete&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;in&nbsp;	(select&nbsp;id&nbsp;from&nbsp;ordiadjust&nbsp;where&nbsp;id&nbsp;>&nbsp;$1&nbsp;order&nbsp;by&nbsp;random()&nbsp;limit&nbsp;$2)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/93_1.sql)
94 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |vacuum&nbsp;verbose&nbsp;public.pgbench\_accounts<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/94_1.sql)
95 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/95_1.sql)
96 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/96_1.sql)
97 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |CREATE&nbsp;DATABASE&nbsp;my\_test\_db<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/97_1.sql)
98 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |ALTER&nbsp;TABLE&nbsp;public.pgbench\_accounts&nbsp;&nbsp;add&nbsp;COLUMN&nbsp;aid\_3&nbsp;integer<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/98_1.sql)
99 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/99_1.sql)
100 |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0<br/>0.00/sec<br/>0.00/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0&nbsp;blks<br/>0.00&nbsp;blks/sec<br/>0.00&nbsp;blks/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;bytes<br/>0.00&nbsp;bytes/sec<br/>0.00&nbsp;bytes/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |0.00&nbsp;ms<br/>0.000ms/sec<br/>0.000ms/call<br/>0.00% |select&nbsp;pg\_sleep($1)<br/><br/>[Full query](../../json_reports/8_2019_05_09T23_10_41_+0300/K_query_groups/100_1.sql)





## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##



## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##



## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##



## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L001"></a>
[Table of contents](#postgres-checkup_top)
# L001 Table Sizes #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



### Master (`127.0.0.1`) ###
  

\# | Table | Rows | &#9660;&nbsp;Total size | Table size | Index(es) Size | TOAST Size
---|---|------|------------|------------|----------------|------------
<no value> | =====TOTAL===== | ~52M | 7631 MB (100.00%) | 6560 MB (100.00%) | 1071 MB (100.00%) | 56 kB (100.00%)
1 | pgbench_accounts | ~50M | 7548 MB (98.92%) | 6477 MB (98.75%) | 1071 MB (99.97%) | <no value>
2 | pgbench_history | ~2M | 82 MB (1.07%) | 82 MB (1.24%) | 0 bytes (0.00%) | <no value>
3 | pgbench_tellers | ~5k | 744 kB (0.01%) | 512 kB (0.01%) | 232 kB (0.02%) | <no value>
4 | pgbench_branches | ~500 | 320 kB (0.00%) | 216 kB (0.00%) | 104 kB (0.01%) | <no value>


## Conclusions ##


## Recommendations ##
---
<a name="postgres-checkup_L003"></a>
[Table of contents](#postgres-checkup_top)
# L003 Integer (int2, int4) Out-of-range Risks in PKs #

## Observations ##
Data collected: 2019-05-09 23:21:39 +0300 MSK  
Current database: test_locks  



No data

## Conclusions ##


## Recommendations ##
